package info.earty.workingpage.domain.model.workingpage;

import info.earty.domain.model.AggregateId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class OutlineItemAdded implements WorkingPageDomainEvent {
    WorkingPageId workingPageId;
    WorkingCardId workingCardId;
    String title;
    Instant occurredOn;

    static OutlineItemAdded create(WorkingPageId workingPageId, WorkingCardId workingCardId, String title) {
        Assert.notNull(workingPageId, OutlineItemAdded.class.getSimpleName() + ": working page id cannot be null");
        Assert.notNull(workingCardId, OutlineItemAdded.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(title, OutlineItemAdded.class.getSimpleName() + ": title cannot be null");
        return new OutlineItemAdded(workingPageId, workingCardId, title, Instant.now());
    }

    @Override
    public AggregateId<WorkingPage> aggregateId() {
        return this.workingPageId;
    }
}
