package info.earty.workingpage.domain.model.workingpage;

public interface WorkingPageIdentityService {
    WorkingPageId generate();
}
