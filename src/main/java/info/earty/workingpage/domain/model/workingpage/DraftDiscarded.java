package info.earty.workingpage.domain.model.workingpage;

import info.earty.domain.model.AggregateId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftDiscarded implements WorkingPageDomainEvent{

    WorkingPageId workingPageId;

    Set<WorkingCardId> priorDraftOutlineItems;
    Set<WorkingCardId> currentDraftOutlineItems;

    Instant occurredOn;

    public Set<WorkingCardId> draftOutlineItemsRemoved() {
        return this.priorDraftOutlineItems.stream().filter(priorPublishedCardId ->
                !currentDraftOutlineItems.contains(priorPublishedCardId)).collect(Collectors.toSet());
    }

    static DraftDiscarded create(WorkingPageId workingPageId, Set<WorkingCardId> priorDraftCardIds,
                                Set<WorkingCardId> currentDraftCardIds) {
        Assert.notNull(workingPageId, DraftDiscarded.class.getSimpleName() + ": working page id cannot be null");
        Assert.notNull(priorDraftCardIds, DraftDiscarded.class.getSimpleName() + ": prior draft card ids cannot be null");
        Assert.notNull(currentDraftCardIds, DraftDiscarded.class.getSimpleName() + ": current draft card ids cannot be null");
        return new DraftDiscarded(workingPageId, new HashSet<>(priorDraftCardIds), new HashSet<>(currentDraftCardIds), Instant.now());
    }

    @Override
    public AggregateId<WorkingPage> aggregateId() {
        return this.workingPageId;
    }
}
