package info.earty.workingpage.domain.model.workingpage;

import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Draft {

    @EqualsAndHashCode.Include
    private final DraftId id;

    private Title title;

    private final Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems;
    private final Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItems;

    private boolean displayItemNumbers;

    private final Map<WorkingCardId, ItemNumber> itemNumbers;

    public final DraftId id() {
        return this.id;
    }

    public Title title() {
        return this.title;
    }

    public DraftOutlineItem rootOutlineItem() {
        return this.findOutlineItem(DraftOutlineItemId.createRootId()).get();
    }

    public Optional<DraftOutlineItem> findOutlineItem(WorkingCardId workingCardId) {
        return this.findOutlineItem(DraftOutlineItemId.create(workingCardId));
    }

    public Set<WorkingCardId> allWorkingCardIds() {
        return this.allOutlineItems.keySet().stream().filter(DraftOutlineItemId::isNonRootId)
                .map(DraftOutlineItemId::workingCardId).collect(Collectors.toSet());
    }

    public boolean displayItemNumbers() {
        return this.displayItemNumbers;
    }

    public ItemNumber outlineItemNumber(WorkingCardId workingCardId) {
        return this.itemNumbers.get(workingCardId);
    }

    public void changeTitle(Title title) {
        this.setTitle(title);
    }

    public void addFragmentToOutlineItem(WorkingCardId workingCardId, Fragment fragment) {
        Assert.notNull(workingCardId, "Error adding a fragment to outline item; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error adding a fragment to outline item; no outline item found for working card id");

        Assert.isTrue(this.allOutlineItems.values().stream().filter(DraftOutlineItem::hasFragment)
                .noneMatch(o -> o.fragment().equals(fragment)), this.getClass().getSimpleName() +
                ": adding this fragment results in duplicate fragments");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.addFragment(fragment);

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.addFragmentToSubItem(workingCardId, fragment);
    }

    public void changeOutlineItemFragment(WorkingCardId workingCardId, Fragment fragment) {
        Assert.notNull(workingCardId, "Error changing the outline item's fragment; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error changing the outline item's fragment; no outline item found for working card id");

        Assert.isTrue(this.allOutlineItems.values().stream().filter(DraftOutlineItem::hasFragment)
                .noneMatch(o -> o.fragment().equals(fragment)), this.getClass().getSimpleName() +
                ": changing this fragment results in duplicate fragments");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.changeFragment(fragment);

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.changeSubItemFragment(workingCardId, fragment);
    }

    public void removeOutlineItemFragment(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error changing the outline item's fragment; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error removing the outline item's fragment; no outline item found for working card id");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.removeFragment();

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.removeSubItemFragment(workingCardId);
    }

    public void moveOutlineItemUp(DraftOutlineItemId parentDraftOutlineItemId, WorkingCardId workingCardId) {
        Assert.notNull(parentDraftOutlineItemId, this.getClass().getSimpleName() + " : parent working card id cannot be null");
        Assert.notNull(workingCardId, this.getClass().getSimpleName() + " : working card id cannot be null");

        DraftOutlineItem parentDraftOutlineItem = this.findOutlineItem(parentDraftOutlineItemId).orElseThrow(() ->
                new IllegalArgumentException(this.getClass().getSimpleName() + " : no parent draft outline item found with id"));
        parentDraftOutlineItem.moveSubItemUp(workingCardId);

        DraftOutlineSubItem movedUpSubItem = parentDraftOutlineItem.findSubItem(workingCardId).get();
        int movedUpSubItemUpdatedPosition = parentDraftOutlineItem.subItems().indexOf(movedUpSubItem);
        DraftOutlineSubItem movedDownSubItem = parentDraftOutlineItem.subItems().get(movedUpSubItemUpdatedPosition + 1);

        Set<DraftOutlineSubItem> movedSubItems = new HashSet<>();
        movedSubItems.add(movedUpSubItem);
        movedSubItems.add(movedDownSubItem);

        determineOutlineItemNumbersForItemsAndSubItems(movedSubItems);
    }

    public void moveOutlineItemDown(DraftOutlineItemId parentDraftOutlineItemId, WorkingCardId workingCardId) {
        Assert.notNull(parentDraftOutlineItemId, this.getClass().getSimpleName() + " : parent working card id cannot be null");
        Assert.notNull(workingCardId, this.getClass().getSimpleName() + " : working card id cannot be null");

        DraftOutlineItem parentDraftOutlineItem = this.findOutlineItem(parentDraftOutlineItemId).orElseThrow(() ->
                new IllegalArgumentException(this.getClass().getSimpleName() + " : no parent draft outline item found with id"));
        parentDraftOutlineItem.moveSubItemDown(workingCardId);

        DraftOutlineSubItem movedDownSubItem = parentDraftOutlineItem.findSubItem(workingCardId).get();
        int movedDownSubItemUpdatedPosition = parentDraftOutlineItem.subItems().indexOf(movedDownSubItem);
        DraftOutlineSubItem movedUpSubItem = parentDraftOutlineItem.subItems().get(movedDownSubItemUpdatedPosition - 1);

        Set<DraftOutlineSubItem> movedSubItems = new HashSet<>();
        movedSubItems.add(movedDownSubItem);
        movedSubItems.add(movedUpSubItem);

        determineOutlineItemNumbersForItemsAndSubItems(movedSubItems);
    }

    public void moveOutlineItemToRoot(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error moving outline item to root; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error moving outline item to root; no outline item found for working card id");

        DraftOutlineItem rootOutlineItem = this.rootOutlineItem();
        Assert.isTrue(rootOutlineItem.findSubItem(workingCardId).isEmpty(),
                "Error moving outline item to root; this outline item already has the root as its parent");

        DraftOutlineItem outlineItem = oOutlineItem.get();

        this.moveOutlineItemToParentHelper(outlineItem, rootOutlineItem);
    }

    public void moveOutlineItemToParent(WorkingCardId workingCardId, WorkingCardId parentId) {
        Assert.notNull(workingCardId, "Error moving outline item to new parent; working card id can't be null");
        Assert.notNull(parentId, "Error moving outline item to new parent; parent id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error moving outline item to new parent; no outline item found for working card id");

        Assert.isTrue(!workingCardId.equals(parentId), this.getClass().getSimpleName() + ": cannot make an outline item its own parent");

        Optional<DraftOutlineItem> oNewParentOutlineItem = this.findOutlineItem(parentId);
        Assert.isTrue(oNewParentOutlineItem.isPresent(),
                "Error moving outline item to new parent; no outline item found for parent id");
        DraftOutlineItem newParentOutlineItem = oNewParentOutlineItem.get();

        Assert.isTrue(newParentOutlineItem.findSubItem(workingCardId).isEmpty(),
                "Error moving outline item to new parent; the proposed new parent is already this outline items parent");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        Assert.isTrue(this.directAndInheritedSubItems(outlineItem).stream().noneMatch(directAndDerivedSubItem ->
                        directAndDerivedSubItem.workingCardId().equals(parentId)),
                "Error moving outline item to new parent; " +
                        "the outline item contains the proposed new parent as a sub-item which introduces an infinite cycle");

        this.moveOutlineItemToParentHelper(outlineItem, newParentOutlineItem);
    }

    public void toggleDisplayItemNumbers() {
        this.displayItemNumbers = !this.displayItemNumbers;
    }

    private void moveOutlineItemToParentHelper(DraftOutlineItem outlineItem, DraftOutlineItem newParentOutlineItem) {
        WorkingCardId workingCardId = outlineItem.id().workingCardId();
        DraftOutlineItem currParentOutlineItem = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        DraftOutlineSubItem subItem = currParentOutlineItem.findSubItem(workingCardId).get();

        //keep track of the items after the one being removed as the item numbers of all of them will need to change
        int removedItemIndex = currParentOutlineItem.subItems().indexOf(subItem);
        Set<DraftOutlineSubItem> afterRemoved = removedItemIndex < currParentOutlineItem.subItems().size() ?
                new HashSet<>(currParentOutlineItem.subItems().subList(removedItemIndex + 1, currParentOutlineItem.subItems().size())) :
                new HashSet<>();

        currParentOutlineItem.removeSubItem(workingCardId);
        newParentOutlineItem.addSubItem(subItem);
        this.parentOutlineItems.put(outlineItem.id(), newParentOutlineItem.id());

        determineOutlineItemNumbersForItemsAndSubItems(Collections.singleton(subItem));
        determineOutlineItemNumbersForItemsAndSubItems(new HashSet<>(afterRemoved));
    }

    private void determineOutlineItemNumbersForItemsAndSubItems(Set<DraftOutlineSubItem> items) {
        while (!items.isEmpty()) {
            Set<DraftOutlineSubItem> subItems = new HashSet<>(items);
            items = new HashSet<>();
            for (DraftOutlineSubItem subItem : subItems) {
                this.itemNumbers.put(subItem.workingCardId(), this.draftOutlineItemNumberHelper(subItem.workingCardId()));
                items.addAll(this.allOutlineItems.get(DraftOutlineItemId.create(subItem.workingCardId())).subItems());
            }
        }
    }

    private ItemNumber draftOutlineItemNumberHelper(WorkingCardId workingCardId) {
        DraftOutlineItem draftOutlineItem = this.findOutlineItem(workingCardId).orElseThrow(() ->
                new IllegalArgumentException(this.getClass().getSimpleName() + " : no draft outline item found with id"));

        DraftOutlineItem parentOutlineItem = this.findOutlineItem(this.parentOutlineItems.get(draftOutlineItem.id())).get();

        if (parentOutlineItem.id().isRootId()) {
            return new ItemNumber(parentOutlineItem.subItems().stream().map(DraftOutlineSubItem::workingCardId).toList()
                    .indexOf(draftOutlineItem.id().workingCardId()) + 1, new ArrayList<>());
        }
        else {
            ItemNumber parentItemNumber = this.itemNumbers.get(parentOutlineItem.id().workingCardId());
            List<Integer> ancestorItemNumbers = parentItemNumber.ancestorNumbers();
            ancestorItemNumbers.add(parentItemNumber.number());

            return new ItemNumber(parentOutlineItem.subItems().stream().map(DraftOutlineSubItem::workingCardId).toList()
                    .indexOf(draftOutlineItem.id().workingCardId()) + 1, ancestorItemNumbers);
        }
    }

    Map<WorkingCardId, ItemNumber> itemNumbers() {
        return new HashMap<>(this.itemNumbers);
    }

    void addOutlineItemToRoot(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error adding outline item to page draft; card id cannot be null");
        Assert.isTrue(this.findOutlineItem(workingCardId).isEmpty(),
                "Error adding outline item to page draft; draft already contains a card with id");

        DraftOutlineItem rootOutlineItem = this.rootOutlineItem();

        DraftOutlineItem outlineItem = DraftOutlineItem.create(workingCardId, title);
        this.allOutlineItems.put(outlineItem.id(), outlineItem);

        rootOutlineItem.addSubItem(workingCardId, title);
        this.parentOutlineItems.put(outlineItem.id(), rootOutlineItem.id());

        ItemNumber itemNumber = new ItemNumber(rootOutlineItem.subItems().size(), new ArrayList<>());
        this.itemNumbers.put(outlineItem.id().workingCardId(), itemNumber);
    }

    void changeOutlineItemTitle(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error changing the outline item's title; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error changing the outline item's title; no outline item found for working card");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.changeTitle(workingCardId, title);

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.changeSubItemTitle(workingCardId, title);
    }

    Set<WorkingCardId> removeOutlineItem(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, this.getClass().getSimpleName() + " : working card id cannot be null");
        Assert.isTrue(this.allOutlineItems.containsKey(DraftOutlineItemId.create(workingCardId)),
                this.getClass().getSimpleName() + " : no outline item found with id");

        DraftOutlineItem parentOutlineItem = this.findOutlineItem(this.parentOutlineItems.get(DraftOutlineItemId.create(workingCardId))).get();

        //keep track of the items after the one being removed as the item numbers of all of them will need to change
        int removedItemIndex = parentOutlineItem.subItems().indexOf(parentOutlineItem.findSubItem(workingCardId).get());
        Set<DraftOutlineSubItem> afterRemoved = removedItemIndex < parentOutlineItem.subItems().size() ?
                new HashSet<>(parentOutlineItem.subItems().subList(removedItemIndex + 1, parentOutlineItem.subItems().size())) :
                new HashSet<>();

        Set<WorkingCardId> removedOutlineItems = this.removeOutlineItemHelper(workingCardId);

        this.determineOutlineItemNumbersForItemsAndSubItems(afterRemoved);

        return removedOutlineItems;
    }

    private Set<WorkingCardId> removeOutlineItemHelper(WorkingCardId workingCardId) {
        DraftOutlineItem outlineItem = this.findOutlineItem(workingCardId).get();

        Set<WorkingCardId> outlineItemsRemoved = new HashSet<>();
        outlineItem.subItems().forEach(s -> {
            outlineItemsRemoved.addAll(this.removeOutlineItemHelper(s.workingCardId()));
        });

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.removeSubItem(workingCardId);

        this.parentOutlineItems.remove(outlineItem.id());
        this.allOutlineItems.remove(outlineItem.id());
        outlineItemsRemoved.add(workingCardId);
        return outlineItemsRemoved;
    }

    static Draft create(DraftId id, Title title, boolean displayItemNumbers) {
        Assert.notNull(id, "Error creating draft page; id cannot be null");

        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems = new HashMap<>();
        allOutlineItems.put(DraftOutlineItemId.createRootId(), DraftOutlineItem.createRoot());

        Draft draft = new Draft(id, allOutlineItems, new HashMap<>(), new HashMap<>());
        draft.setTitle(title);
        draft.setDisplayItemNumbers(displayItemNumbers);
        return draft;
    }

    static Draft create(DraftId id, PublishedPage publishedPage) {
        Assert.notNull(id, "Error creating draft page; id cannot be null");
        Assert.notNull(publishedPage, "Error creating draft page; published page cannot be null");

        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems =
                draftItemsFromPublishedRoot(publishedPage.rootOutlineItem(), publishedPage);
        Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItems = new HashMap<>();

        allOutlineItems.values().forEach(draftOutlineItem -> draftOutlineItem.subItems().forEach(draftOutlineSubItem ->
                parentOutlineItems.put(DraftOutlineItemId.create(draftOutlineSubItem.workingCardId()), draftOutlineItem.id())));

        Draft draft = new Draft(id, allOutlineItems, parentOutlineItems, publishedPage.itemNumbers());
        draft.setTitle(publishedPage.title());
        draft.setDisplayItemNumbers(publishedPage.displayItemNumbers());
        return draft;
    }

    private Optional<DraftOutlineItem> findOutlineItem(DraftOutlineItemId draftOutlineItemId) {
        return this.allOutlineItems.containsKey(draftOutlineItemId) ?
                Optional.of(this.allOutlineItems.get(draftOutlineItemId)) : Optional.empty();
    }

    private static Map<DraftOutlineItemId, DraftOutlineItem> draftItemsFromPublishedRoot(PublishedOutlineItem publishedOutlineItem, PublishedPage publishedPage)  {
        Map<DraftOutlineItemId, DraftOutlineItem> draftOutlineItems = new HashMap<>();
        DraftOutlineItem draftOutlineItem = DraftOutlineItem.create(publishedOutlineItem);
        draftOutlineItems.put(publishedOutlineItem.draftOutlineItemId(), draftOutlineItem);

        publishedOutlineItem.subItems().forEach(
                publishedOutlineSubItem -> {
                    draftOutlineItems.putAll(draftItemsFromPublishedRoot(publishedPage.findOutlineItem(
                            publishedOutlineSubItem.workingCardId()).get(), publishedPage));
                });
        return draftOutlineItems;
    }

    private Collection<DraftOutlineSubItem> directAndInheritedSubItems(DraftOutlineItem outlineItem) {
        Collection<DraftOutlineSubItem> directAndInheritedSubItems = new HashSet<>(outlineItem.subItems());
        outlineItem.subItems().forEach(subItem ->
            directAndInheritedSubItems.addAll(this.directAndInheritedSubItems(
                    this.findOutlineItem(subItem.workingCardId()).get())));
        return directAndInheritedSubItems;
    }

    private void setTitle(Title title) {
        Assert.notNull(title, this.getClass().getSimpleName() + ": title cannot be null");
        this.title = title;
    }

    private void setDisplayItemNumbers(boolean displayItemNumbers) {
        this.displayItemNumbers = displayItemNumbers;
    }
}