package info.earty.workingpage.domain.model.workingcard;

public interface WorkingCardIdentityService {
    WorkingCardId generate();
}
