package info.earty.workingpage.domain.model.workingpage;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
@Accessors(fluent = true)
public class ItemNumber {

    int number;
    List<Integer> ancestorNumbers;

    ItemNumber(int number, List<Integer> ancestorNumbers) {
        Assert.isTrue(number >= 1, this.getClass().getSimpleName() + " : number must be 1 or greater");
        Assert.notNull(ancestorNumbers, this.getClass().getSimpleName() + " : ancestor numbers cannot be null");
        Assert.isTrue(ancestorNumbers.stream().allMatch(i -> i >= 1), this.getClass().getSimpleName() + " : ancestor item numbers must be 1 or greater");
        this.number = number;
        this.ancestorNumbers = new ArrayList<>(ancestorNumbers);
    }

    public int level() {
        return this.ancestorNumbers().size() + 1;
    }

    public String hierarchyNumber(String delimiter) {
        List<String> fullHierarchyList = this.ancestorNumbers().stream().map(Object::toString).collect(Collectors.toList());
        fullHierarchyList.add(String.valueOf(number));

        return String.join(delimiter, fullHierarchyList) + delimiter;
    }

    public List<Integer> ancestorNumbers() {
        return new ArrayList<>(this.ancestorNumbers);
    }

}
