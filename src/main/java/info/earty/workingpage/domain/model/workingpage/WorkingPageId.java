package info.earty.workingpage.domain.model.workingpage;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.PartnerAggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class WorkingPageId implements AggregateId<WorkingPage>, PartnerAggregateId {
    String id;

    @Override
    public Class<WorkingPage> aggregateType() {
        return WorkingPage.class;
    }
}
