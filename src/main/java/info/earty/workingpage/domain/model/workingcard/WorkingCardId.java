package info.earty.workingpage.domain.model.workingcard;

import info.earty.domain.model.PartnerAggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class WorkingCardId implements PartnerAggregateId {
    String id;
}
