package info.earty.workingpage.domain.model.workingpage;

import info.earty.domain.model.DomainEvent;


public interface WorkingPageDomainEvent extends DomainEvent<WorkingPage> {

}
