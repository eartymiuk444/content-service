package info.earty.workingpage.domain.model.workingpage;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Title {

    String titleString;

    public static Title create(String titleString) {
        Assert.notNull(titleString, "title cannot be null");
        Assert.isTrue(titleString.length() > 0, "title must include at least one character");
        return new Title(titleString);
    }

}
