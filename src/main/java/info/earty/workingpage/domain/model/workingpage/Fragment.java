package info.earty.workingpage.domain.model.workingpage;

import info.earty.workingpage.domain.model.common.Rfc3986Patterns;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class Fragment {

    String fragmentString;

    public Fragment(String fragmentString) {
        Assert.notNull(fragmentString, "Error creating fragment; fragment string cannot be null");
        Assert.isTrue(Rfc3986Patterns.FRAGMENT_PATTERN.matcher(fragmentString).matches(), this.getClass().getSimpleName() + ": invalid fragment string");
        this.fragmentString = fragmentString;
    }
}
