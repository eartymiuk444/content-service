package info.earty.workingpage.domain.model.sitemenu;

import info.earty.domain.model.PartnerAggregateId;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@Accessors(fluent = true)
public class SiteMenuId implements PartnerAggregateId {
    String id;
}
