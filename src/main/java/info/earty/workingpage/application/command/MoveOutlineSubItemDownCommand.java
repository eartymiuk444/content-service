package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class MoveOutlineSubItemDownCommand {
    private String workingPageId;
    private String parentWorkingCardId;
    private String workingCardId;
}
