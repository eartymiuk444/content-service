package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class DiscardDraftCommand {
    private String workingPageId;
}
