package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class ToggleDisplayItemNumbersCommand {
    private String workingPageId;
}
