package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class RemoveOutlineItemCommand {

    private String workingPageId;
    private String workingCardId;

}
