package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class ChangeTitleCommand {
    private String workingPageId;
    private String title;
}
