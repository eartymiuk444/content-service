package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class PublishCommand {
    private String workingPageId;
}
