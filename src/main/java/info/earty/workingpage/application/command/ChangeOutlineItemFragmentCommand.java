package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class ChangeOutlineItemFragmentCommand {
    private String workingPageId;
    private String workingCardId;
    private String fragment;
}
