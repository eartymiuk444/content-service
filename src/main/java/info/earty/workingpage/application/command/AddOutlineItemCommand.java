package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class AddOutlineItemCommand {
    private String workingPageId;
}
