package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class CreateCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
    private String title;
}
