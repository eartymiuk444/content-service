package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class RemoveCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
}
