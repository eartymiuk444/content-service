package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class ChangeOutlineItemTitleCommand {

    private String workingPageId;
    private String workingCardId;
    private String title;

}
