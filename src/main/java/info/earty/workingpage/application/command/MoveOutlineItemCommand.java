package info.earty.workingpage.application.command;

import lombok.Data;

@Data
public class MoveOutlineItemCommand {
    private String workingPageId;
    private String workingCardId;
    private Boolean moveToRoot;
    private String parentWorkingCardId;
}
