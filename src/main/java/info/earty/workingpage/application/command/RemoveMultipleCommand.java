package info.earty.workingpage.application.command;

import lombok.Data;

import java.util.Set;

@Data
public class RemoveMultipleCommand {
    private int eventId;
    private String siteMenuId;
    private Set<String> workingPageIds;
}
