package info.earty.workingpage.application;

import info.earty.application.*;
import info.earty.application.aspect.UnitOfWork;
import info.earty.domain.model.BasicRepository;
import info.earty.domain.model.DomainEventPublisher;
import info.earty.workingpage.application.command.*;
import info.earty.workingpage.domain.model.sitemenu.SiteMenuId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardIdentityService;
import info.earty.workingpage.domain.model.workingpage.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WorkingPageCommandService {

    private final WorkingPageFactory workingPageFactory;

    private final BasicRepository<WorkingPage> workingPageRepository;
    private final WorkingCardIdentityService workingCardIdentityService;

    private final InboxRepository<WorkingPage> workingPageInboxRepository;

    private final OutboxRepository<WorkingPage> workingPageOutboxRepository;

    private final UnitOfWorkProvider unitOfWorkProvider;

    private static final String NEW_CARD_TITLE = "Title";

    @UnitOfWork
    public void createPage(CreateCommand aCommand) { //event command
        SiteMenuId siteMenuId = new SiteMenuId(aCommand.getSiteMenuId());
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        if (!workingPageRepository.existsById(workingPageId)) { //deduplication
            WorkingPage workingPage = workingPageFactory.create(siteMenuId, workingPageId, Title.create(aCommand.getTitle()));
            workingPageRepository.add(workingPage);

            Inbox<WorkingPage> workingPageInbox = new Inbox<>(workingPageId);
            workingPageInbox.eventProcessed(new SiteMenuId(aCommand.getSiteMenuId()), aCommand.getEventId());
            workingPageInboxRepository.add(workingPageInbox);

            workingPageOutboxRepository.add(new Outbox<>(workingPageId));
        }
    }

    @UnitOfWork
    public void removePage(RemoveCommand aCommand) { //event command
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        Optional<WorkingPage> oWorkingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()));

        if (oWorkingPage.isPresent()) { //deduplication
            workingPageRepository.remove(oWorkingPage.get());
            workingPageOutboxRepository.remove(workingPageOutboxRepository.findByAggregateId(workingPageId).get());
            workingPageInboxRepository.remove(workingPageInboxRepository.findByAggregateId(workingPageId).get());
        }
    }

    @UnitOfWork(outboxEnqueuers = WorkingPage.class)
    public void addOutlineItem(AddOutlineItemCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException("Error adding card to working page; working page with id not found"));
        WorkingCardId workingCardId = workingCardIdentityService.generate();
        workingPage.addOutlineItemToRoot(workingCardId, NEW_CARD_TITLE);
    }

    @UnitOfWork
    public void changeOutlineItemFragment(ChangeOutlineItemFragmentCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException("Error changing outline item fragment; working page with id not found"));

        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        if (aCommand.getFragment() == null || aCommand.getFragment().equals("")) {
            workingPage.draft().removeOutlineItemFragment(workingCardId);
        }
        else {
            DraftOutlineItem draftOutlineItem = workingPage.draft().findOutlineItem(workingCardId)
                    .orElseThrow(() -> new IllegalArgumentException(
                            "Error changing outline item fragment; no draft outline item found with working card id"));
            Fragment fragment = new Fragment(aCommand.getFragment());

            if (draftOutlineItem.hasFragment()) {
                workingPage.draft().changeOutlineItemFragment(workingCardId, fragment);
            }
            else {
                workingPage.draft().addFragmentToOutlineItem(workingCardId, fragment);
            }
        }
    }

    @UnitOfWork(outboxEnqueuers = WorkingPage.class)
    public void removeOutlineItem(RemoveOutlineItemCommand aCommand) {
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingPage workingPage = workingPageRepository.findById(workingPageId).orElseThrow(
                () -> new IllegalArgumentException("Error removing outline item; working page with id not found"));

        workingPage.removeOutlineItem(workingCardId);
    }

    @UnitOfWork
    public void moveOutlineItem(MoveOutlineItemCommand aCommand) {
        Assert.isTrue(aCommand.getMoveToRoot() && aCommand.getParentWorkingCardId() == null ||
                !aCommand.getMoveToRoot() && aCommand.getParentWorkingCardId() != null,
                "Error moving outline item; mismatch between 'move to root' and 'parent working card id'");

        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingPage workingPage = workingPageRepository.findById(workingPageId).orElseThrow(
                () -> new IllegalArgumentException("Error removing outline item; working page with id not found"));

        if (aCommand.getMoveToRoot()) {
            workingPage.draft().moveOutlineItemToRoot(workingCardId);
        }
        else {
            WorkingCardId parentWorkingCardId = new WorkingCardId(aCommand.getParentWorkingCardId());
            workingPage.draft().moveOutlineItemToParent(workingCardId, parentWorkingCardId);
        }

    }

    @UnitOfWork
    public void moveOutlineSubItemUp(MoveOutlineSubItemUpCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error moving outline item up; working page with id not found"));
        DraftOutlineItemId parentDraftOutlineItemId;
        if (aCommand.getParentWorkingCardId() == null || aCommand.getParentWorkingCardId().equals("")) {
            parentDraftOutlineItemId = DraftOutlineItemId.createRootId();
        }
        else {
            parentDraftOutlineItemId = DraftOutlineItemId.create(new WorkingCardId(aCommand.getParentWorkingCardId()));
        }
        workingPage.draft().moveOutlineItemUp(parentDraftOutlineItemId, new WorkingCardId(aCommand.getWorkingCardId()));
    }

    @UnitOfWork
    public void moveOutlineSubItemDown(MoveOutlineSubItemDownCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error moving outline item down; working page with id not found"));
        DraftOutlineItemId parentDraftOutlineItemId;
        if (aCommand.getParentWorkingCardId() == null || aCommand.getParentWorkingCardId().equals("")) {
            parentDraftOutlineItemId = DraftOutlineItemId.createRootId();
        }
        else {
            parentDraftOutlineItemId = DraftOutlineItemId.create(new WorkingCardId(aCommand.getParentWorkingCardId()));
        }
        workingPage.draft().moveOutlineItemDown(parentDraftOutlineItemId, new WorkingCardId(aCommand.getWorkingCardId()));
    }

    @UnitOfWork(outboxEnqueuers = WorkingPage.class)
    public void discardDraft(DiscardDraftCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error discarding page draft; working page with id not found"));
        workingPage.discardDraft();
    }

    @UnitOfWork(outboxEnqueuers = WorkingPage.class)
    public void publish(PublishCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error publishing page; working page with id not found"));
        workingPage.publish();
    }

    @UnitOfWork
    public void changeTitle(ChangeTitleCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error changing page's title; working page with id not found"));
        workingPage.draft().changeTitle(Title.create(aCommand.getTitle()));
    }

    @UnitOfWork(outboxEnqueuers = WorkingPage.class)
    public void changeOutlineItemTitle(ChangeOutlineItemTitleCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error changing outline item's title; working page with id not found"));
        workingPage.changeOutlineItemTitle(new WorkingCardId(aCommand.getWorkingCardId()), aCommand.getTitle());
    }

    public void removePages(RemoveMultipleCommand aCommand) {
        aCommand.getWorkingPageIds().stream().map(WorkingPageId::new).forEach(this::removePage);
    }

    //@ApplicationServiceCommand(aggregateType = WorkingPage.class)
    //TODO EA 8/27/2021 - Use AspectJ
    private void removePage(WorkingPageId workingPageId) {
        DomainEventPublisher.instance().reset();
        info.earty.application.UnitOfWork uow = unitOfWorkProvider.start();
        try {
            Optional<WorkingPage> oWorkingPage = workingPageRepository.findById(workingPageId);

            if (oWorkingPage.isPresent()) { //deduplication
                workingPageRepository.remove(oWorkingPage.get());
                workingPageOutboxRepository.remove(workingPageOutboxRepository.findByAggregateId(workingPageId).get());
                workingPageInboxRepository.remove(workingPageInboxRepository.findByAggregateId(workingPageId).get());
            }
            uow.commit();
        } catch (RuntimeException e) {
            uow.rollback();
            throw e;
        }
    }

    @UnitOfWork
    public void toggleDisplayItemNumbers(ToggleDisplayItemNumbersCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error changing outline item's title; working page with id not found"));
        workingPage.draft().toggleDisplayItemNumbers();
    }
}
