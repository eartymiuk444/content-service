package info.earty.workingpage.infrastructure.eartyinfo;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.domain.model.PartnerAggregateId;
import info.earty.infrastructure.mongo.MongoDocumentAdapter;
import info.earty.workingpage.domain.model.sitemenu.SiteMenuId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import info.earty.workingpage.domain.model.workingpage.*;
import info.earty.workingpage.infrastructure.mongo.MongoDraft;
import info.earty.workingpage.infrastructure.mongo.MongoPublishedPage;
import info.earty.workingpage.infrastructure.mongo.MongoWorkingPage;
import org.bson.Document;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

import static info.earty.infrastructure.reflection.ReflectionHelpers.*;

@Component
public class MongoWorkingPageAdapter extends MongoDocumentAdapter<MongoWorkingPage, WorkingPage> {

    private static final String ROOT_DRAFT_OUTLINE_ITEM_ID = "ROOT_DRAFT_OUTLINE_ITEM_ID";
    private final MappingMongoConverter mappingMongoConverter;

    protected MongoWorkingPageAdapter(MappingMongoConverter mappingMongoConverter) {
        super(MongoWorkingPage.class, WorkingPage.class);
        this.mappingMongoConverter = mappingMongoConverter;
    }

    @Override
    public WorkingPage reconstituteAggregate(MongoWorkingPage document) {
        Assert.notNull(document, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(document.isHasAggregate(), "Error reconstituting aggregate; the document does not contain an aggregate instance");

        WorkingPage workingPage = newInstance(accessibleConstructor(WorkingPage.class, WorkingPageId.class, SiteMenuId.class),
                new WorkingPageId(document.getId()), document.getSiteMenuId());

        setField(workingPage, "publishedPage", reconstitutePublishedPage(document.getPublishedPage()));
        setField(workingPage, "draft", reconstituteDraft(document.getDraft()));
        setField(workingPage, "lastPublished", document.getLastPublished());
        setField(workingPage, "nextDraftIdInt", document.getNextDraftIdInt());

        return workingPage;
    }

    @Override
    protected AggregateId<WorkingPage> reconstituteAggregateId(String s) {
        return new WorkingPageId(s);
    }

    @Override
    protected PartnerAggregateId reconstitutePartnerId(Class<? extends PartnerAggregateId> partnerIdType, String partnerKey) {
        Assert.notNull(partnerIdType, this.getClass().getSimpleName() + " : partner id type cannot be null");
        Assert.notNull(partnerKey, this.getClass().getSimpleName() + " : partner key cannot be null");

        if (partnerIdType.equals(SiteMenuId.class)) {
            return new SiteMenuId(partnerKey);
        }
        else if (partnerIdType.equals(WorkingCardId.class)) {
            return new WorkingCardId(partnerKey);
        }
        else {
            throw new IllegalArgumentException(this.getClass().getSimpleName() + " : unknown partner type");
        }
    }

    @Override
    protected DomainEvent<WorkingPage> reconstituteDomainEvent(Document bsonDomainEvent) {
        return (DomainEvent<WorkingPage>) mappingMongoConverter.read(
                mappingMongoConverter.getTypeMapper().readType(bsonDomainEvent).getType(), bsonDomainEvent);
    }

    private PublishedPage reconstitutePublishedPage(MongoPublishedPage mongoPublishedPage) {

        Map<DraftOutlineItemId, PublishedOutlineItem> allOutlineItems = new HashMap<>();
        mongoPublishedPage.getAllOutlineItems().forEach((draftOutlineItemIdString, publishedOutlineItem) -> {
            DraftOutlineItemId draftOutlineItemId = draftOutlineItemIdString.equals(ROOT_DRAFT_OUTLINE_ITEM_ID) ? DraftOutlineItemId.createRootId() :
                    DraftOutlineItemId.create(new WorkingCardId(draftOutlineItemIdString));
            allOutlineItems.put(draftOutlineItemId, publishedOutlineItem);
        });

        Map<WorkingCardId, ItemNumber> itemNumbers = new HashMap<>();
        mongoPublishedPage.getItemNumbers().forEach((workingCardIdString, itemNumber) -> {
            itemNumbers.put(new WorkingCardId(workingCardIdString), itemNumber);
        });

        return newInstance(accessibleConstructor(PublishedPage.class, Title.class, Map.class, boolean.class, Map.class),
                mongoPublishedPage.getTitle(), allOutlineItems, mongoPublishedPage.isDisplayItemNumbers(), itemNumbers);
    }

    private Draft reconstituteDraft(MongoDraft mongoDraft) {

        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems = new HashMap<>();
        mongoDraft.getAllOutlineItems().forEach((draftOutlineItemIdString, draftOutlineItem) -> {
            DraftOutlineItemId draftOutlineItemId = draftOutlineItemIdString.equals(ROOT_DRAFT_OUTLINE_ITEM_ID) ? DraftOutlineItemId.createRootId() :
                    DraftOutlineItemId.create(new WorkingCardId(draftOutlineItemIdString));
            allOutlineItems.put(draftOutlineItemId, draftOutlineItem);
        });

        Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItems = new HashMap<>();
        mongoDraft.getParentOutlineItems().forEach((draftOutlineItemIdString, parentId) -> {
            DraftOutlineItemId draftOutlineItemId = DraftOutlineItemId.create(new WorkingCardId(draftOutlineItemIdString));
            parentOutlineItems.put(draftOutlineItemId, parentId);
        });

        Map<WorkingCardId, ItemNumber> itemNumbers = new HashMap<>();
        mongoDraft.getItemNumbers().forEach((workingCardIdString, itemNumber) -> {
            itemNumbers.put(new WorkingCardId(workingCardIdString), itemNumber);
        });

        Draft draft = newInstance(accessibleConstructor(Draft.class, DraftId.class, Map.class, Map.class, Map.class),
                mongoDraft.getId(), allOutlineItems, parentOutlineItems, itemNumbers);
        setField(draft, "title", mongoDraft.getTitle());
        setField(draft, "displayItemNumbers", mongoDraft.isDisplayItemNumbers());
        return draft;
    }
}
