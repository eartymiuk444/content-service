package info.earty.workingpage.infrastructure.eartyinfo;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.domain.model.PartnerAggregateId;
import info.earty.infrastructure.mongo.AggregateAdapter;
import info.earty.workingpage.domain.model.sitemenu.SiteMenuId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import info.earty.workingpage.domain.model.workingpage.*;
import info.earty.workingpage.infrastructure.mongo.MongoDraft;
import info.earty.workingpage.infrastructure.mongo.MongoPublishedPage;
import info.earty.workingpage.infrastructure.mongo.MongoWorkingPage;
import org.bson.Document;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

import static info.earty.infrastructure.reflection.ReflectionHelpers.getField;

@Component
public class WorkingPageAdapter extends AggregateAdapter<WorkingPage, MongoWorkingPage> {

    private static final String ROOT_DRAFT_OUTLINE_ITEM_ID = "ROOT_DRAFT_OUTLINE_ITEM_ID";

    private final MappingMongoConverter mappingMongoConverter;

    protected WorkingPageAdapter(MappingMongoConverter mappingMongoConverter) {
        super(WorkingPage.class, MongoWorkingPage.class);
        this.mappingMongoConverter = mappingMongoConverter;
    }

    @Override
    protected String adaptIdToString(AggregateId<WorkingPage> aggregateId) {
        Assert.isTrue(aggregateId instanceof WorkingPageId, this.getClass().getSimpleName() + " : unexpected aggregate id type");
        WorkingPageId workingPageId = (WorkingPageId) aggregateId;

        return workingPageId.id();
    }

    @Override
    protected String adaptPartnerIdToString(PartnerAggregateId partnerAggregateId) {
        if (partnerAggregateId instanceof SiteMenuId) {
            return ((SiteMenuId)partnerAggregateId).id();
        }
        else if (partnerAggregateId instanceof WorkingCardId) {
            return ((WorkingCardId)partnerAggregateId).id();
        }
        else {
            throw new IllegalArgumentException(this.getClass().getSimpleName() + " : unexpected partner id type");
        }
    }

    @Override
    protected Document adaptDomainEventToBson(DomainEvent<WorkingPage> domainEvent) {
        Document document = new Document();
        mappingMongoConverter.write(domainEvent, document);
        return document;
    }

    @Override
    protected void clearAggregateInternal(MongoWorkingPage document) {
        document.setSiteMenuId(null);
        document.setLastPublished(null);
        document.setPublishedPage(null);
        document.setDraft(null);
        document.setNextDraftIdInt(0);

    }

    @Override
    protected void setAggregateInternal(WorkingPage aggregate, MongoWorkingPage document) {
        //domain provides read access
        document.setSiteMenuId(aggregate.siteMenuId());
        document.setLastPublished(aggregate.lastPublished());
        document.setPublishedPage(toDocument(aggregate.publishedPage()));
        document.setDraft(toDocument(aggregate.draft()));

        //domain restricts read access
        //nextDraftIdInt
        document.setNextDraftIdInt(getField(aggregate, "nextDraftIdInt", Integer.class));
    }

    private MongoPublishedPage toDocument(PublishedPage publishedPage) {
        MongoPublishedPage mongoPublishedPage = new MongoPublishedPage();
        mongoPublishedPage.setTitle(publishedPage.title());

        Map<String, PublishedOutlineItem> allOutlineItems = new HashMap<>();
        Map<DraftOutlineItemId, PublishedOutlineItem> allOutlineItemsDomain = getField(publishedPage, "allOutlineItems", Map.class);
        allOutlineItemsDomain.forEach(((draftOutlineItemId, publishedOutlineItem) ->
                allOutlineItems.put(draftOutlineItemId.isRootId() ? ROOT_DRAFT_OUTLINE_ITEM_ID :
                        draftOutlineItemId.workingCardId().id(), publishedOutlineItem)));
        mongoPublishedPage.setAllOutlineItems(allOutlineItems);

        mongoPublishedPage.setDisplayItemNumbers(publishedPage.displayItemNumbers());

        Map<String, ItemNumber> itemNumbers = new HashMap<>();
        Map<WorkingCardId, ItemNumber> itemNumbersDomain = getField(publishedPage, "itemNumbers", Map.class);
        itemNumbersDomain.forEach((workingCardId, itemNumber) -> itemNumbers.put(workingCardId.id(), itemNumber));
        mongoPublishedPage.setItemNumbers(itemNumbers);

        return mongoPublishedPage;
    }

    private MongoDraft toDocument(Draft draft) {
        MongoDraft mongoDraft = new MongoDraft();
        mongoDraft.setId(draft.id());
        mongoDraft.setTitle(draft.title());

        Map<String, DraftOutlineItem> allOutlineItems = new HashMap<>();
        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItemsDomain = getField(draft, "allOutlineItems", Map.class);
        allOutlineItemsDomain.forEach(((draftOutlineItemId, draftOutlineItem) ->
                allOutlineItems.put(draftOutlineItemId.isRootId() ? ROOT_DRAFT_OUTLINE_ITEM_ID :
                        draftOutlineItemId.workingCardId().id(), draftOutlineItem)));
        mongoDraft.setAllOutlineItems(allOutlineItems);

        mongoDraft.setDisplayItemNumbers(draft.displayItemNumbers());

        Map<String, DraftOutlineItemId> parentOutlineItems = new HashMap<>();
        Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItemsDomain = getField(draft, "parentOutlineItems", Map.class);
        parentOutlineItemsDomain.forEach(((draftOutlineItemId, parentId) ->
                parentOutlineItems.put(draftOutlineItemId.isRootId() ? ROOT_DRAFT_OUTLINE_ITEM_ID :
                        draftOutlineItemId.workingCardId().id(), parentId)));
        mongoDraft.setParentOutlineItems(parentOutlineItems);

        Map<String, ItemNumber> itemNumbers = new HashMap<>();
        Map<WorkingCardId, ItemNumber> itemNumbersDomain = getField(draft, "itemNumbers", Map.class);
        itemNumbersDomain.forEach((workingCardId, itemNumber) -> itemNumbers.put(workingCardId.id(), itemNumber));
        mongoDraft.setItemNumbers(itemNumbers);

        return mongoDraft;
    }

    @Override
    protected MongoWorkingPage newDocumentInstance() {
        return new MongoWorkingPage();
    }
}
