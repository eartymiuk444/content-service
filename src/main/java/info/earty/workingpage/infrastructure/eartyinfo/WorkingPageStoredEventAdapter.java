package info.earty.workingpage.infrastructure.eartyinfo;

import info.earty.application.StoredEvent;
import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.infrastructure.jms.spring.StoredEventAdapter;
import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import info.earty.workingpage.domain.model.workingpage.*;
import info.earty.workingpage.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WorkingPageStoredEventAdapter implements StoredEventAdapter<WorkingPage> {

    @Override
    public String adaptAggregateIdToString(AggregateId<WorkingPage> aggregateId) {
        Assert.isTrue(aggregateId instanceof WorkingPageId, this.getClass().getSimpleName() + " : unexpected aggregate id type");
        WorkingPageId workingPageId = (WorkingPageId) aggregateId;

        return workingPageId.id();
    }

    @Override
    public Object adaptToMessageConvertibleObject(StoredEvent<WorkingPage> storedEvent) {
        DomainEvent<WorkingPage> domainEvent = storedEvent.domainEvent();

        if (domainEvent instanceof DraftDiscarded) {
            DraftDiscarded draftDiscarded = (DraftDiscarded)domainEvent;
            DraftDiscardedJsonDto dto = new DraftDiscardedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingPageId(draftDiscarded.workingPageId().id());
            dto.setOccurredOn(draftDiscarded.occurredOn());

            dto.setPriorDraftCardIds(draftDiscarded.priorDraftOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setCurrentDraftCardIds(draftDiscarded.currentDraftOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setDraftCardIdsRemoved(draftDiscarded.draftOutlineItemsRemoved().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));

            return dto;
        }
        else if (domainEvent instanceof DraftOutlineItemTitleChanged) {
            DraftOutlineItemTitleChanged draftOutlineItemTitleChanged = (DraftOutlineItemTitleChanged) domainEvent;
            DraftOutlineItemTitleChangedJsonDto dto = new DraftOutlineItemTitleChangedJsonDto();

            dto.setId(storedEvent.id());
            dto.setOccurredOn(draftOutlineItemTitleChanged.occurredOn());
            dto.setWorkingPageId(draftOutlineItemTitleChanged.workingPageId().id());

            dto.setWorkingCardId(draftOutlineItemTitleChanged.workingCardId().id());
            dto.setTitle(draftOutlineItemTitleChanged.title());

            return dto;
        }
        else if (domainEvent instanceof OutlineItemAdded) {
            OutlineItemAdded outlineItemAdded = (OutlineItemAdded) domainEvent;
            OutlineItemAddedJsonDto dto = new OutlineItemAddedJsonDto();

            dto.setId(storedEvent.id());
            dto.setOccurredOn(outlineItemAdded.occurredOn());
            dto.setWorkingPageId(outlineItemAdded.workingPageId().id());

            dto.setWorkingCardId(outlineItemAdded.workingCardId().id());
            dto.setTitle(outlineItemAdded.title());

            return dto;
        }
        else if (domainEvent instanceof OutlineItemRemoved) {
            OutlineItemRemoved outlineItemRemoved = (OutlineItemRemoved) domainEvent;
            OutlineItemRemovedJsonDto dto = new OutlineItemRemovedJsonDto();

            dto.setId(storedEvent.id());
            dto.setOccurredOn(outlineItemRemoved.occurredOn());
            dto.setWorkingPageId(outlineItemRemoved.workingPageId().id());

            dto.setPublishedOutlineItems(outlineItemRemoved.publishedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setDraftOutlineItemsRemoved(outlineItemRemoved.draftOutlineItemsRemoved().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setOrphanedOutlineItems(outlineItemRemoved.orphanedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));

            return dto;
        }
        else if (domainEvent instanceof PagePublished) {
            PagePublished pagePublished = (PagePublished) domainEvent;
            PagePublishedJsonDto dto = new PagePublishedJsonDto();

            dto.setId(storedEvent.id());
            dto.setSiteMenuId(pagePublished.siteMenuId().id());
            dto.setWorkingPageId(pagePublished.workingPageId().id());
            dto.setOccurredOn(pagePublished.occurredOn());

            dto.setPublishedTitle(pagePublished.publishedTitle().titleString());
            dto.setPriorPublishedCardIds(pagePublished.priorPublishedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setCurrentPublishedCardIds(pagePublished.currentPublishedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setPublishedCardIdsRemoved(pagePublished.publishedOutlineItemsRemoved().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            return dto;
        }
        else {
            throw new IllegalArgumentException("Error adapting working page stored event to jms message convertible object; " +
                    "working page domain event is an unknown type");
        }
    }
}
