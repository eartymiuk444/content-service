package info.earty.workingpage.infrastructure.eartyinfo;

import info.earty.infrastructure.mongo.MongoDocumentRepository;
import info.earty.workingpage.domain.model.workingpage.WorkingPage;
import info.earty.workingpage.infrastructure.mongo.MongoWorkingPage;

public interface MongoWorkingPageRepository extends MongoDocumentRepository<WorkingPage, MongoWorkingPage> {
}
