package info.earty.workingpage.infrastructure.uuid;

import info.earty.workingpage.domain.model.workingcard.WorkingCardId;
import info.earty.workingpage.domain.model.workingcard.WorkingCardIdentityService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDWorkingCardIdentityService implements WorkingCardIdentityService {
    @Override
    public WorkingCardId generate() {
        return new WorkingCardId(UUID.randomUUID().toString());
    }
}
