package info.earty.workingpage.infrastructure.mongo;

import info.earty.workingpage.domain.model.workingpage.ItemNumber;
import info.earty.workingpage.domain.model.workingpage.PublishedOutlineItem;
import info.earty.workingpage.domain.model.workingpage.Title;
import lombok.Data;

import java.util.Map;

@Data
public class MongoPublishedPage {
    private Title title;
    private Map<String, PublishedOutlineItem> allOutlineItems;
    private boolean displayItemNumbers;
    private Map<String, ItemNumber> itemNumbers;
}
