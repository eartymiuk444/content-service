package info.earty.workingpage.infrastructure.mongo;

import info.earty.infrastructure.mongo.Document;
import info.earty.workingpage.domain.model.sitemenu.SiteMenuId;
import info.earty.workingpage.domain.model.workingpage.WorkingPage;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document("workingPage")
public class MongoWorkingPage extends Document<WorkingPage> {

    private SiteMenuId siteMenuId;
    private MongoPublishedPage publishedPage;
    private MongoDraft draft;
    private Instant lastPublished;
    private int nextDraftIdInt;

}
