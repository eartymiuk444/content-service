package info.earty.workingpage.infrastructure.mongo;

import info.earty.workingpage.domain.model.workingpage.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MongoDraft {
    @EqualsAndHashCode.Include
    private DraftId id;
    private Title title;
    private Map<String, DraftOutlineItem> allOutlineItems;
    private Map<String, DraftOutlineItemId> parentOutlineItems;
    private boolean displayItemNumbers;
    private Map<String, ItemNumber> itemNumbers;
}
