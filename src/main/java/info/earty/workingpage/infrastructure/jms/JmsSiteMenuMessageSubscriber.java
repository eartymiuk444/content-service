package info.earty.workingpage.infrastructure.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.infrastructure.autoconfigure.infrastructure.InfrastructureProperties;
import info.earty.sitemenu.infrastructure.jms.dto.DirectoryRemovedJsonDto;
import info.earty.sitemenu.infrastructure.jms.dto.PageAddedJsonDto;
import info.earty.sitemenu.infrastructure.jms.dto.PageRemovedJsonDto;
import info.earty.workingpage.application.WorkingPageCommandService;
import info.earty.workingpage.application.command.CreateCommand;
import info.earty.workingpage.application.command.RemoveCommand;
import info.earty.workingpage.application.command.RemoveMultipleCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import java.util.Map;

@Component("workingPageSiteMenuMessageSubscriber")
@RequiredArgsConstructor
public class JmsSiteMenuMessageSubscriber {

    private final ObjectMapper objectMapper;

    private final WorkingPageCommandService workingPageCommandService;

    @JmsListener(destination = "info.earty.site-menu", subscription = "info.earty.site-menu.working-page")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(InfrastructureProperties.DEFAULT_JMS_TYPE_ID_PROPERTY);

        if (type.equals(PageAddedJsonDto.class.getName())) {
            PageAddedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PageAddedJsonDto.class);

            CreateCommand createCommand = new CreateCommand();
            createCommand.setEventId(dto.getId());
            createCommand.setSiteMenuId(dto.getSiteMenuId());

            createCommand.setWorkingPageId(dto.getWorkingPageId());
            createCommand.setTitle(dto.getName());
            workingPageCommandService.createPage(createCommand);
        }
        else if (type.equals(PageRemovedJsonDto.class.getName())) {
            PageRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PageRemovedJsonDto.class);

            RemoveCommand removeCommand = new RemoveCommand();
            removeCommand.setEventId(dto.getId());
            removeCommand.setSiteMenuId(dto.getSiteMenuId());

            removeCommand.setWorkingPageId(dto.getWorkingPageId());
            workingPageCommandService.removePage(removeCommand);
        }
        else if (type.equals(DirectoryRemovedJsonDto.class.getName())) {
            DirectoryRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), DirectoryRemovedJsonDto.class);

            RemoveMultipleCommand removeMultipleCommand = new RemoveMultipleCommand();
            removeMultipleCommand.setEventId(dto.getId());
            removeMultipleCommand.setSiteMenuId(dto.getSiteMenuId());

            removeMultipleCommand.setWorkingPageIds(dto.getPagesRemoved());
            workingPageCommandService.removePages(removeMultipleCommand);
        }
    }

}
