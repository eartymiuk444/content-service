package info.earty.workingpage.presentation;

import info.earty.workingpage.presentation.data.DraftJsonDto;
import info.earty.workingpage.presentation.data.PublishedPageJsonDto;
import info.earty.workingpage.application.WorkingPageQueryService;
import info.earty.workingpage.application.data.DraftDto;
import info.earty.workingpage.application.data.PublishedPageDto;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WorkingPageQueryApiService implements WorkingPageQueryApi {

    private final WorkingPageQueryService workingPageQueryService;
    private final DtoMapper dtoMapper;

    @Override
    public DraftJsonDto getDraft(String workingPageId) {
        return dtoMapper.map(workingPageQueryService.getDraft(workingPageId));
    }

    @Override
    public PublishedPageJsonDto getPublishedPage(String workingPageId) {
        return dtoMapper.map(workingPageQueryService.getPublishedPage(workingPageId));
    }

    @Mapper(componentModel = "spring")
    interface DtoMapper {
        DraftJsonDto map(DraftDto dto);
        DraftJsonDto.OutlineItemDto map(DraftDto.OutlineItemDto dto);
        DraftJsonDto.OutlineSubItemDto map(DraftDto.OutlineSubItemDto dto);
        PublishedPageJsonDto map(PublishedPageDto dto);
        PublishedPageJsonDto.OutlineItemDto map(PublishedPageDto.OutlineItemDto dto);
        PublishedPageJsonDto.OutlineSubItemDto map(PublishedPageDto.OutlineSubItemDto dto);
    }
}
