package info.earty.attachment.domain.model.attachment;

import java.util.Optional;

public interface AttachmentRepository {
    void add(Attachment attachment);
    Optional<Attachment> findById(AttachmentId attachmentId);
    void remove(AttachmentId attachmentId);
}
