package info.earty.attachment.domain.model.attachment;

import info.earty.attachment.domain.model.workingcard.WorkingCardId;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.InputStream;

@Component
public class AttachmentFactory {

    public Attachment create(AttachmentId attachmentId, WorkingCardId workingCardId, String fileName, String mediaType, InputStream inputStream) {
        Assert.notNull(attachmentId, this.getClass().getSimpleName() + "attachment id cannot be null");

        Attachment attachment = new Attachment(attachmentId);
        attachment.setWorkingCardId(workingCardId);
        attachment.setFilename(fileName);
        attachment.setContentType(mediaType);
        attachment.setInputStream(inputStream);
        attachment.setPublished(false);

        return attachment;
    }

}
