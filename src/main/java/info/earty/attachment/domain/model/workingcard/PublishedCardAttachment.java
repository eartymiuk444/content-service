package info.earty.attachment.domain.model.workingcard;

import info.earty.attachment.domain.model.attachment.AttachmentId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishedCardAttachment {

    WorkingCardId workingCardId;
    AttachmentId attachmentId;

    public static PublishedCardAttachment create(String workingCardId, String attachmentId) {
        return new PublishedCardAttachment(WorkingCardId.create(workingCardId), AttachmentId.create(attachmentId));
    }

}
