package info.earty.attachment.domain.model.workingcard;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class CardAttachment {

    WorkingCardId workingCardId;

    public static CardAttachment create(String workingCardId) {
        return new CardAttachment(WorkingCardId.create(workingCardId));
    }

}
