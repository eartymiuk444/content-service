package info.earty.attachment.domain.model.attachment;

public interface AttachmentIdentityService {
    AttachmentId generate();
}
