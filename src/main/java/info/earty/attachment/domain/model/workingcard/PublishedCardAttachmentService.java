package info.earty.attachment.domain.model.workingcard;

import info.earty.attachment.domain.model.attachment.AttachmentId;

public interface PublishedCardAttachmentService {

    PublishedCardAttachment publishedCardAttachmentFrom(WorkingCardId workingCardId, AttachmentId attachmentId);

}
