package info.earty.attachment.domain.model.workingcard;

public interface CardAttachmentService {

    CardAttachment cardAttachmentFrom(WorkingCardId workingCardId);

}
