package info.earty.attachment.infrastructure.google.cloud.storage;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.collect.Lists;
import info.earty.attachment.application.AttachmentUnitOfWorkService;
import info.earty.attachment.domain.model.attachment.Attachment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class GoogleStorageAttachmentUnitOfWorkService implements AttachmentUnitOfWorkService {

    private static final String GOOGLE_APIS_AUTH_ENDPOINT = "https://www.googleapis.com/auth/cloud-platform";

    @Value("${info.earty.attachment.service.google-storage.json-key}")
    private String googleStorageJsonKey;

    @Value("${info.earty.attachment.service.google-storage.bucket-name}")
    private String googleStorageBucketName;

    @Override
    public void saveAttachmentAsPublished(Attachment attachment) {
        Assert.isTrue(attachment.published(), this.getClass().getSimpleName() + " : attachment is not published");

        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
            BlobId blobId = BlobId.of(googleStorageBucketName, attachment.id().id());
            Blob blob = storage.get(blobId);
            if (blob != null) {
                Map<String, String> publishedMetadata = new HashMap<>();
                publishedMetadata.put("published", Boolean.TRUE.toString());
                blob.toBuilder().setMetadata(publishedMetadata).build().update();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
