package info.earty.attachment.infrastructure.google.cloud.storage;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import com.google.common.collect.Lists;
import info.earty.attachment.domain.model.attachment.Attachment;
import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.attachment.AttachmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AttachmentRepositoryAdapter implements AttachmentRepository {

    private static final String GOOGLE_APIS_AUTH_ENDPOINT = "https://www.googleapis.com/auth/cloud-platform";

    @Value("${info.earty.attachment.service.google-storage.json-key}")
    private String googleStorageJsonKey;

    @Value("${info.earty.attachment.service.google-storage.bucket-name}")
    private String googleStorageBucketName;

    private final BlobAttachmentAdapter blobAttachmentAdapter;

    @Override
    public void add(Attachment attachment) {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

            BlobId blobId = BlobId.of(googleStorageBucketName, attachment.id().id());

            Map<String, String> metadata = new HashMap<>();
            metadata.put("workingCardId", attachment.workingCardId().id());
            metadata.put("filename", attachment.filename());
            metadata.put("published", attachment.published() ? Boolean.TRUE.toString() : Boolean.FALSE.toString());

            BlobInfo blobInfo = BlobInfo.newBuilder(blobId)
                    .setContentType(attachment.contentType())
                    .setMetadata(metadata)
                    .build();

            storage.createFrom(blobInfo, attachment.inputStream());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Attachment> findById(AttachmentId attachmentId) {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
            BlobId blobId = BlobId.of(googleStorageBucketName, attachmentId.id());
            Blob blob = storage.get(blobId);

            return blob == null ? Optional.empty() : Optional.of(blobAttachmentAdapter.reconstituteAggregate(blob));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove(AttachmentId attachmentId) {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
            BlobId blobId = BlobId.of(googleStorageBucketName, attachmentId.id());
            storage.delete(blobId);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
