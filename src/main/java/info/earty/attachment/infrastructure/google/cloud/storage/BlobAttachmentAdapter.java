package info.earty.attachment.infrastructure.google.cloud.storage;

import com.google.cloud.storage.Blob;
import info.earty.attachment.domain.model.attachment.Attachment;
import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.workingcard.WorkingCardId;
import info.earty.infrastructure.reflection.ReflectionHelpers;
import org.springframework.stereotype.Component;

import java.nio.channels.Channels;

import static info.earty.infrastructure.reflection.ReflectionHelpers.accessibleConstructor;
import static info.earty.infrastructure.reflection.ReflectionHelpers.setField;

//TODO EA 4/3/2022 - Consider using the builder/loader pattern instead of reflection
@Component
public class BlobAttachmentAdapter {

    public Attachment reconstituteAggregate(Blob blob) {
        Attachment attachment = ReflectionHelpers.newInstance(accessibleConstructor(Attachment.class, AttachmentId.class), AttachmentId.create(blob.getBlobId().getName()));
        setField(attachment, "filename", blob.getMetadata().get("filename"));
        setField(attachment, "workingCardId", WorkingCardId.create(blob.getMetadata().get("workingCardId")));
        setField(attachment, "published", blob.getMetadata().get("published").equals(Boolean.TRUE.toString()));
        setField(attachment, "contentType", blob.getContentType());
        setField(attachment, "inputStream", Channels.newInputStream(blob.reader()));
        return attachment;
    }

}
