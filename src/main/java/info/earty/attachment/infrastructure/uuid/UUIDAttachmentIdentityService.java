package info.earty.attachment.infrastructure.uuid;

import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.attachment.AttachmentIdentityService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDAttachmentIdentityService implements AttachmentIdentityService {
    @Override
    public AttachmentId generate() {
        return AttachmentId.create(UUID.randomUUID().toString());
    }
}
