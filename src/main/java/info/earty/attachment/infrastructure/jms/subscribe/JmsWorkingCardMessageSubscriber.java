package info.earty.attachment.infrastructure.jms.subscribe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.attachment.application.DeleteCommand;
import info.earty.attachment.application.AttachmentCommandService;
import info.earty.attachment.application.PublishCommand;
import info.earty.infrastructure.autoconfigure.infrastructure.InfrastructureProperties;
import info.earty.workingcard.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component("attachmentWorkingCardMessageSubscriber")
@RequiredArgsConstructor
public class JmsWorkingCardMessageSubscriber {

    private final ObjectMapper objectMapper;

    private final AttachmentCommandService attachmentCommandService;

    @JmsListener(destination = "info.earty.working-card", subscription = "info.earty.working-card.attachment")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(InfrastructureProperties.DEFAULT_JMS_TYPE_ID_PROPERTY);

        if (type.equals(AddAttachmentFailedJsonDto.class.getName())) {
            AddAttachmentFailedJsonDto dto = objectMapper.readValue(message.getBody(String.class), AddAttachmentFailedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            Set<String> attachmentIds = new HashSet<>();
            attachmentIds.add(dto.getAttachmentId());
            deleteCommand.setAttachmentIds(attachmentIds);
            attachmentCommandService.delete(deleteCommand);

        }
        else if (type.equals(AttachmentRemovedJsonDto.class.getName())) {
            AttachmentRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), AttachmentRemovedJsonDto.class);
            if (dto.isAttachmentOrphaned()) {
                DeleteCommand deleteCommand = new DeleteCommand();
                Set<String> attachmentIds = new HashSet<>();
                attachmentIds.add(dto.getAttachmentRemoved());
                deleteCommand.setAttachmentIds(attachmentIds);
                attachmentCommandService.delete(deleteCommand);
            }
        }
        else if (type.equals(CardPublishedJsonDto.class.getName())) {
            CardPublishedJsonDto dto = objectMapper.readValue(message.getBody(String.class), CardPublishedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            deleteCommand.setAttachmentIds(new HashSet<>(dto.getPublishedAttachmentsRemoved()));
            attachmentCommandService.delete(deleteCommand);

            PublishCommand publishCommand = new PublishCommand();
            publishCommand.setAttachmentIds(new HashSet<>(dto.getPublishedAttachmentsAdded()));
            attachmentCommandService.publish(publishCommand);

        }
        else if (type.equals(CardDraftDiscardedJsonDto.class.getName())) {
            CardDraftDiscardedJsonDto dto = objectMapper.readValue(message.getBody(String.class), CardDraftDiscardedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            deleteCommand.setAttachmentIds(new HashSet<>(dto.getDraftAttachmentsRemoved()));
            attachmentCommandService.delete(deleteCommand);

        }
        else if (type.equals(CardDeletedJsonDto.class.getName())) {
            CardDeletedJsonDto dto = objectMapper.readValue(message.getBody(String.class), CardDeletedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            deleteCommand.setAttachmentIds(new HashSet<>(dto.getOrphanedAttachments()));
            attachmentCommandService.delete(deleteCommand);

        }
        else if (type.equals(ImageRemovedJsonDto.class.getName())) {
            //no-op
        }
        else if (type.equals(AddImageFailedJsonDto.class.getName())) {
            //no-op
        }
        else {
            throw new IllegalArgumentException("Error receiving working-card message; unknown message type");
        }
    }

}
