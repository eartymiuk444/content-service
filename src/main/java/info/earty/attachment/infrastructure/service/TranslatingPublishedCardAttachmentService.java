package info.earty.attachment.infrastructure.service;

import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.workingcard.PublishedCardAttachment;
import info.earty.attachment.domain.model.workingcard.PublishedCardAttachmentService;
import info.earty.attachment.domain.model.workingcard.WorkingCardId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TranslatingPublishedCardAttachmentService implements PublishedCardAttachmentService {

    private final WorkingCardAdapter workingCardAdapter;

    @Override
    public PublishedCardAttachment publishedCardAttachmentFrom(WorkingCardId workingCardId, AttachmentId attachmentId) {
        return workingCardAdapter.toPublishedCardAttachment(workingCardId, attachmentId);
    }
}
