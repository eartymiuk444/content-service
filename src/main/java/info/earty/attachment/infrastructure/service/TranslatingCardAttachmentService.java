package info.earty.attachment.infrastructure.service;

import info.earty.attachment.domain.model.workingcard.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TranslatingCardAttachmentService implements CardAttachmentService {

    private final WorkingCardAdapter workingCardAdapter;

    @Override
    public CardAttachment cardAttachmentFrom(WorkingCardId workingCardId) {
        return workingCardAdapter.toCardAttachment(workingCardId);
    }
}
