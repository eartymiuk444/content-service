package info.earty.attachment.infrastructure.service;

import info.earty.workingcard.presentation.data.DraftCardJsonDto;
import info.earty.attachment.domain.model.workingcard.CardAttachment;
import org.springframework.stereotype.Component;

@Component
public class CardAttachmentTranslator {

    public CardAttachment toCardAttachmentFromRepresentation(DraftCardJsonDto draftCardJsonDto) {
        return CardAttachment.create(draftCardJsonDto.getWorkingCardId());
    }


}
