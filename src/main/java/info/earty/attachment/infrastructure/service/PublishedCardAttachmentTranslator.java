package info.earty.attachment.infrastructure.service;

import info.earty.workingcard.presentation.data.PublishedCardJsonDto;
import info.earty.attachment.domain.model.workingcard.PublishedCardAttachment;
import org.springframework.stereotype.Component;

@Component
public class PublishedCardAttachmentTranslator {

    public PublishedCardAttachment toPublishedCardAttachmentFromRepresentation(String workingCardId, PublishedCardJsonDto.Attachment publishedCardJsonDtoAttachment) {
        return PublishedCardAttachment.create(workingCardId, publishedCardJsonDtoAttachment.getAttachmentId());
    }


}
