package info.earty.attachment.infrastructure.service;

import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.workingcard.CardAttachment;
import info.earty.attachment.domain.model.workingcard.PublishedCardAttachment;
import info.earty.attachment.domain.model.workingcard.WorkingCardId;

public interface WorkingCardAdapter {

    PublishedCardAttachment toPublishedCardAttachment(WorkingCardId workingCardId, AttachmentId attachmentId);
    CardAttachment toCardAttachment(WorkingCardId workingCardId);

}
