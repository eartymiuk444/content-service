package info.earty.attachment.infrastructure.service;

import info.earty.workingcard.presentation.WorkingCardQueryApi;
import info.earty.workingcard.presentation.data.DraftCardJsonDto;
import info.earty.workingcard.presentation.data.PublishedCardJsonDto;
import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.workingcard.CardAttachment;
import info.earty.attachment.domain.model.workingcard.PublishedCardAttachment;
import info.earty.attachment.domain.model.workingcard.WorkingCardId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import jakarta.ws.rs.WebApplicationException;
import java.util.Optional;

@Component("attachmentHttpWorkingCardAdapter")
@RequiredArgsConstructor
public class HttpWorkingCardAdapter implements WorkingCardAdapter {

    private final WorkingCardQueryApi workingCardQueryApi;
    private final PublishedCardAttachmentTranslator publishedCardAttachmentTranslator;
    private final CardAttachmentTranslator cardAttachmentTranslator;

    @Override
    public PublishedCardAttachment toPublishedCardAttachment(WorkingCardId workingCardId, AttachmentId attachmentId) {
        PublishedCardJsonDto publishedCardJsonDto;
        try {
            publishedCardJsonDto = workingCardQueryApi.getPublishedCard(workingCardId.id());
        } catch (WebApplicationException e) {
            //NOTE EA 2022-04-18 if we get back a client error then we know we tried to retrieve a published card that doesn't exist.
            if (e.getResponse().getStatus() >= 400 && e.getResponse().getStatus() < 500) {
                return null;
            }
            else {
                throw e;
            }
        }

        Optional<PublishedCardJsonDto.Attachment> optionalAttachment = publishedCardJsonDto.getAttachments().stream().filter(
                attachment -> attachment.getAttachmentId().equals(attachmentId.id())).findFirst();

        return optionalAttachment.map(attachment -> publishedCardAttachmentTranslator.toPublishedCardAttachmentFromRepresentation(
                publishedCardJsonDto.getWorkingCardId(), attachment)).orElse(null);
    }

    @Override
    public CardAttachment toCardAttachment(WorkingCardId workingCardId) {
        DraftCardJsonDto draftCardJsonDto;
        try {
            draftCardJsonDto = workingCardQueryApi.getDraftCard(workingCardId.id());
        } catch (WebApplicationException e) {
            //NOTE EA 2022-04-18 if we get back a client error then we know we tried to retrieve a published card that doesn't exist.
            if (e.getResponse().getStatus() >= 400 && e.getResponse().getStatus() < 500) {
                return null;
            }
            else {
                throw e;
            }
        }

        return cardAttachmentTranslator.toCardAttachmentFromRepresentation(draftCardJsonDto);
    }
}
