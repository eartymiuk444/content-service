package info.earty.attachment.presentation;

import info.earty.attachment.application.CreateCommand;
import info.earty.attachment.application.AttachmentCommandService;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class AttachmentCommandApiService implements AttachmentCommandApi {

    private final AttachmentCommandService attachmentCommandService;

    @Override
    public void create(Attachment attachment, String workingCardId) {
        CreateCommand aCommand = new CreateCommand();
        aCommand.setWorkingCardId(workingCardId);
        aCommand.setFilename(attachment.getContentDisposition().getFilename());
        aCommand.setContentType(attachment.getContentType().toString());

        try {
            aCommand.setInputStream(attachment.getDataHandler().getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        attachmentCommandService.create(aCommand);
    }

}
