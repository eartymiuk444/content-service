package info.earty.attachment.presentation;

import info.earty.attachment.application.AttachmentQueryService;
import info.earty.attachment.domain.model.attachment.Attachment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

@Service
@RequiredArgsConstructor
public class AttachmentQueryApiService implements AttachmentQueryApi {

    private final AttachmentQueryService attachmentQueryService;

    @Override
    public Response get(String attachmentId) {
        Attachment attachment = attachmentQueryService.get(attachmentId);
        return Response.ok(attachment.inputStream())
                .header(HttpHeaders.CONTENT_TYPE, attachment.contentType())
                .build();
    }

    @Override
    public Response get(String attachmentId, String filename) {
        Attachment attachment = attachmentQueryService.get(attachmentId, filename);
        return Response.ok(attachment.inputStream())
                .header(HttpHeaders.CONTENT_TYPE, attachment.contentType())
                .build();
    }

    @Override
    public Response getPublished(String attachmentId, String workingCardId) {
        Attachment attachment = attachmentQueryService.getPublished(attachmentId, workingCardId);
        return Response.ok(attachment.inputStream())
                .header(HttpHeaders.CONTENT_TYPE, attachment.contentType())
                .build();
    }

    @Override
    public Response getPublished(String attachmentId, String filename, String workingCardId) {
        Attachment attachment = attachmentQueryService.getPublished(attachmentId, filename, workingCardId);
        return Response.ok(attachment.inputStream())
                .header(HttpHeaders.CONTENT_TYPE, attachment.contentType())
                .build();
    }
}
