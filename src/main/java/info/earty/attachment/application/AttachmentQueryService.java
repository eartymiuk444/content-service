package info.earty.attachment.application;

import info.earty.attachment.domain.model.attachment.Attachment;
import info.earty.attachment.domain.model.attachment.AttachmentId;
import info.earty.attachment.domain.model.attachment.AttachmentRepository;
import info.earty.attachment.domain.model.workingcard.PublishedCardAttachmentService;
import info.earty.attachment.domain.model.workingcard.WorkingCardId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttachmentQueryService {

    private final AttachmentRepository attachmentRepository;
    private final PublishedCardAttachmentService publishedCardAttachmentService;

    public Attachment get(String attachmentId) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(AttachmentId.create(attachmentId));
        return optionalAttachment.orElseThrow(() -> new IllegalArgumentException(
                this.getClass().getSimpleName() + " : no attachment found with id"));
    }

    public Attachment get(String attachmentId, String filename) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(AttachmentId.create(attachmentId));
        Assert.isTrue(optionalAttachment.isPresent() && optionalAttachment.get().filename().equals(filename),
                this.getClass().getSimpleName() + " : no attachment found with id and filename");
        return optionalAttachment.get();
    }

    public Attachment getPublished(String attachmentId, String workingCardId) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(AttachmentId.create(attachmentId));
        if (optionalAttachment.isPresent()) {
            Attachment attachment = optionalAttachment.get();

            if (attachment.published() || publishedCardAttachmentService.publishedCardAttachmentFrom(
                    WorkingCardId.create(workingCardId), AttachmentId.create(attachmentId)) != null) {
                return attachment;
            }
        }

        throw new IllegalArgumentException(this.getClass().getSimpleName() + " : no published attachment found with id");
    }

    public Attachment getPublished(String attachmentId, String filename, String workingCardId) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(AttachmentId.create(attachmentId));
        if (optionalAttachment.isPresent() && optionalAttachment.get().filename().equals(filename)) {
            Attachment attachment = optionalAttachment.get();

            if (attachment.published() || publishedCardAttachmentService.publishedCardAttachmentFrom(
                    WorkingCardId.create(workingCardId), AttachmentId.create(attachmentId)) != null) {
                return attachment;
            }
        }

        throw new IllegalArgumentException(this.getClass().getSimpleName() + " : no published attachment found with id and filename");
    }

}
