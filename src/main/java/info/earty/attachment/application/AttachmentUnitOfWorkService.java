package info.earty.attachment.application;

import info.earty.attachment.domain.model.attachment.Attachment;

public interface AttachmentUnitOfWorkService {

    void saveAttachmentAsPublished(Attachment attachment);

}
