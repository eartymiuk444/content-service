package info.earty.attachment.application;

import lombok.Data;

import java.util.Set;

@Data
public class PublishCommand {

    private Set<String> attachmentIds;

}
