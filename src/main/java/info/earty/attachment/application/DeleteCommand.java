package info.earty.attachment.application;

import lombok.Data;

import java.util.Set;

@Data
public class DeleteCommand {

    private Set<String> attachmentIds;

}
