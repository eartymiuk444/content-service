package info.earty.attachment.application;

import info.earty.attachment.domain.model.attachment.*;
import info.earty.attachment.domain.model.workingcard.WorkingCardId;
import info.earty.attachment.domain.model.workingcard.CardAttachmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttachmentCommandService {

    private final AttachmentIdentityService attachmentIdentityService;
    private final AttachmentFactory attachmentFactory;
    private final AttachmentRepository attachmentRepository;
    private final AttachmentUnitOfWorkService attachmentUnitOfWorkService;
    private final CardAttachmentService cardAttachmentService;

    public void create(CreateCommand aCommand) {
        Assert.notNull(cardAttachmentService.cardAttachmentFrom(WorkingCardId.create(aCommand.getWorkingCardId())),
                this.getClass().getSimpleName() + " : no working card found with id");
        Attachment attachment = attachmentFactory.create(attachmentIdentityService.generate(), WorkingCardId.create(aCommand.getWorkingCardId()),
                aCommand.getFilename(), aCommand.getContentType(), aCommand.getInputStream());
        attachmentRepository.add(attachment);
    }

    public void delete(DeleteCommand aCommand) { //event command
        for (String attachmentId : aCommand.getAttachmentIds()) {
            attachmentRepository.remove(AttachmentId.create(attachmentId));
        }
    }

    public void publish(PublishCommand aCommand) { //event command
        for (String attachmentId : aCommand.getAttachmentIds()) {

            Optional<Attachment> optionalAttachment = attachmentRepository.findById(AttachmentId.create(attachmentId));
            if (optionalAttachment.isPresent()) {
                Attachment attachment = optionalAttachment.get();
                attachment.publish();
                attachmentUnitOfWorkService.saveAttachmentAsPublished(attachment);
            }
        }
    }

}
