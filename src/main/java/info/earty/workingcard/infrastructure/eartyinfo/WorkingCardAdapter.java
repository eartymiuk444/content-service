package info.earty.workingcard.infrastructure.eartyinfo;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.domain.model.PartnerAggregateId;
import info.earty.infrastructure.mongo.AggregateAdapter;
import info.earty.workingcard.domain.model.card.WorkingCard;
import info.earty.workingcard.domain.model.card.WorkingCardId;
import info.earty.workingcard.domain.model.sitemenu.SiteMenuId;
import info.earty.workingcard.domain.model.workingpage.WorkingPageId;
import info.earty.workingcard.infrastructure.mongo.MongoWorkingCard;
import org.bson.Document;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import static info.earty.infrastructure.reflection.ReflectionHelpers.getField;

@Component
public class WorkingCardAdapter extends AggregateAdapter<WorkingCard, MongoWorkingCard> {

    private final MappingMongoConverter mappingMongoConverter;

    public WorkingCardAdapter(MappingMongoConverter mappingMongoConverter) {
        super(WorkingCard.class, MongoWorkingCard.class);
        this.mappingMongoConverter = mappingMongoConverter;
    }

    @Override
    protected String adaptIdToString(AggregateId<WorkingCard> aggregateId) {
        Assert.isTrue(aggregateId instanceof WorkingCardId, this.getClass().getSimpleName() + " : unexpected aggregate id type");
        WorkingCardId workingCardId = (WorkingCardId) aggregateId;

        return workingCardId.id();
    }

    @Override
    protected String adaptPartnerIdToString(PartnerAggregateId partnerId) {
        if (partnerId instanceof WorkingPageId) {
            WorkingPageId workingPageId = (WorkingPageId) partnerId;
            return workingPageId.id();
        }
        else if (partnerId instanceof SiteMenuId) {
            SiteMenuId siteMenuId = (SiteMenuId) partnerId;
            return siteMenuId.id();
        }
        else {
            throw new IllegalArgumentException(this.getClass().getSimpleName() + " : unexpected partner id type");
        }
    }

    @Override
    protected Document adaptDomainEventToBson(DomainEvent<WorkingCard> workingCardDomainEvent) {
        org.bson.Document document = new org.bson.Document();
        mappingMongoConverter.write(workingCardDomainEvent, document);
        return document;
    }

    @Override
    protected void clearAggregateInternal(MongoWorkingCard document) {
        document.setHasAggregate(false);
        document.setWorkingPageId(null);
        document.setPublishedCard(null);
        document.setDraftCard(null);
        document.setNextDraftCardIdInt(0);
    }

    @Override
    protected void setAggregateInternal(WorkingCard aggregate, MongoWorkingCard document) {
        //domain provides read access
        document.setWorkingPageId(aggregate.workingPageId());
        document.setPublishedCard(aggregate.publishedCard());
        document.setDraftCard(aggregate.draftCard());

        //domain restricts read access
        document.setNextDraftCardIdInt(getField(aggregate, "nextDraftCardIdInt", Integer.class));
    }

    @Override
    protected MongoWorkingCard newDocumentInstance() {
        return new MongoWorkingCard();
    }
}
