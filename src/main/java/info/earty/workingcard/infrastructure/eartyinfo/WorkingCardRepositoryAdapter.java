package info.earty.workingcard.infrastructure.eartyinfo;

import info.earty.application.UnitOfWorkProvider;
import info.earty.infrastructure.mongo.AggregateAdapter;
import info.earty.infrastructure.mongo.MongoBasicRepositoryAdapter;
import info.earty.infrastructure.mongo.MongoDocumentAdapter;
import info.earty.workingcard.domain.model.card.WorkingCard;
import info.earty.workingcard.domain.model.card.WorkingCardRepository;
import info.earty.workingcard.domain.model.workingpage.WorkingPageId;
import info.earty.workingcard.infrastructure.mongo.MongoWorkingCard;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class WorkingCardRepositoryAdapter extends MongoBasicRepositoryAdapter<WorkingCard, MongoWorkingCard> implements WorkingCardRepository {

    public WorkingCardRepositoryAdapter(UnitOfWorkProvider unitOfWorkProvider,
                                        MongoWorkingCardRepository mongoWorkingCardRepository,
                                        MongoDocumentAdapter<MongoWorkingCard, WorkingCard> mongoDocumentAdapter,
                                        AggregateAdapter<WorkingCard, MongoWorkingCard> aggregateAdapter) {
        super(unitOfWorkProvider, mongoWorkingCardRepository, mongoDocumentAdapter, aggregateAdapter);
    }

    @Override
    public Set<WorkingCard> findByWorkingPageId(WorkingPageId workingPageId) {
        Assert.notNull(workingPageId, this.getClass().getSimpleName() + " : working page id cannot be null");
        Assert.isTrue(!super.getUnitOfWorkProvider().exists(), "Error retrieving working cards by working page id; " +
                "a unit of work exists and multiple cards cannot be worked on as a unit");

        return  ((MongoWorkingCardRepository)super.getMongoDocumentRepository()).findByWorkingPageId(workingPageId)
                .stream().map((mongoWorkingCard) -> super.getMongoDocumentAdapter().reconstituteAggregate(mongoWorkingCard)).collect(Collectors.toSet());
    }
}
