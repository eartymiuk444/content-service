package info.earty.workingcard.infrastructure.eartyinfo;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.domain.model.PartnerAggregateId;
import info.earty.infrastructure.mongo.MongoDocumentAdapter;
import info.earty.workingcard.domain.model.card.WorkingCard;
import info.earty.workingcard.domain.model.card.WorkingCardId;
import info.earty.workingcard.domain.model.sitemenu.SiteMenuId;
import info.earty.workingcard.domain.model.workingpage.WorkingPageId;
import info.earty.workingcard.infrastructure.mongo.MongoWorkingCard;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import static info.earty.infrastructure.reflection.ReflectionHelpers.*;

@Component
public class MongoWorkingCardAdapter extends MongoDocumentAdapter<MongoWorkingCard, WorkingCard> {

    private final MappingMongoConverter mappingMongoConverter;

    public MongoWorkingCardAdapter(MappingMongoConverter mappingMongoConverter) {
        super(MongoWorkingCard.class, WorkingCard.class);
        this.mappingMongoConverter = mappingMongoConverter;
    }

    @Override
    public WorkingCard reconstituteAggregate(MongoWorkingCard document) {
        Assert.notNull(document, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(document.isHasAggregate(), "Error reconstituting aggregate; the document does not contain an aggregate instance");

        WorkingCardId workingCardId = new WorkingCardId(document.getId());

        accessibleConstructor(WorkingCard.class, WorkingCardId.class, WorkingPageId.class);
        WorkingCard workingCard = newInstance(accessibleConstructor(WorkingCard.class, WorkingCardId.class, WorkingPageId.class),
                workingCardId, document.getWorkingPageId());

        setField(workingCard, "publishedCard", document.getPublishedCard());
        setField(workingCard, "draftCard", document.getDraftCard());
        setField(workingCard, "nextDraftCardIdInt", document.getNextDraftCardIdInt());

        return workingCard;
    }

    @Override
    protected AggregateId<WorkingCard> reconstituteAggregateId(String documentKey) {
        Assert.notNull(documentKey, this.getClass().getSimpleName() + " : document key cannot be null");
        return new WorkingCardId(documentKey);
    }

    @Override
    protected PartnerAggregateId reconstitutePartnerId(Class<? extends PartnerAggregateId> partnerIdType, String partnerKey) {
        Assert.notNull(partnerIdType, this.getClass().getSimpleName() + " : partner id type cannot be null");
        Assert.notNull(partnerKey, this.getClass().getSimpleName() + " : partner key cannot be null");

        if (partnerIdType.equals(WorkingPageId.class)) {
            return new WorkingPageId(partnerKey);
        }
        else if (partnerIdType.equals(SiteMenuId.class)) {
            return new SiteMenuId(partnerKey);
        }
        else {
            throw new IllegalArgumentException(this.getClass().getSimpleName() + " : unexpected parnter id type");
        }
    }

    @Override
    protected DomainEvent<WorkingCard> reconstituteDomainEvent(org.bson.Document bsonDomainEvent) {
        return (DomainEvent<WorkingCard>) mappingMongoConverter.read(
                mappingMongoConverter.getTypeMapper().readType(bsonDomainEvent).getType(), bsonDomainEvent);
    }
}
