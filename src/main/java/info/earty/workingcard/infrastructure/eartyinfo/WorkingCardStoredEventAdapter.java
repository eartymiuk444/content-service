package info.earty.workingcard.infrastructure.eartyinfo;

import info.earty.application.StoredEvent;
import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.infrastructure.jms.spring.StoredEventAdapter;
import info.earty.workingcard.domain.model.card.*;
import info.earty.workingcard.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.stream.Collectors;

import info.earty.workingcard.domain.model.image.ImageId;
import info.earty.workingcard.domain.model.attachment.AttachmentId;

@Component
@RequiredArgsConstructor
public class WorkingCardStoredEventAdapter implements StoredEventAdapter<WorkingCard> {

    @Override
    public String adaptAggregateIdToString(AggregateId<WorkingCard> aggregateId) {
        Assert.isTrue(aggregateId instanceof WorkingCardId, this.getClass().getSimpleName() + " : unexpected aggregate id type");
        WorkingCardId workingCardId = (WorkingCardId) aggregateId;

        return workingCardId.id();
    }

    @Override
    public Object adaptToMessageConvertibleObject(StoredEvent<WorkingCard> storedEvent) {
        DomainEvent<WorkingCard> domainEvent = storedEvent.domainEvent();

        if (domainEvent instanceof AddImageFailed) {
            AddImageFailed addImageFailed = (AddImageFailed) domainEvent;
            AddImageFailedJsonDto dto = new AddImageFailedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(addImageFailed.workingCardId().id());
            dto.setImageId(addImageFailed.imageId().id());
            dto.setOccurredOn(addImageFailed.occurredOn());

            return dto;
        }
        else if (domainEvent instanceof AddAttachmentFailed) {
            AddAttachmentFailed addAttachmentFailed = (AddAttachmentFailed) domainEvent;
            AddAttachmentFailedJsonDto dto = new AddAttachmentFailedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(addAttachmentFailed.workingCardId().id());
            dto.setAttachmentId(addAttachmentFailed.attachmentId().id());
            dto.setOccurredOn(addAttachmentFailed.occurredOn());

            return dto;
        }
        else if (domainEvent instanceof ImageRemoved) {
            ImageRemoved imageRemoved = (ImageRemoved) domainEvent;
            ImageRemovedJsonDto dto = new ImageRemovedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(imageRemoved.workingCardId().id());
            dto.setImageRemoved(imageRemoved.imageRemoved().id());
            dto.setImageOrphaned(imageRemoved.imageOrphaned());
            dto.setOccurredOn(imageRemoved.occurredOn());

            return dto;
        }
        else if (domainEvent instanceof AttachmentRemoved) {
            AttachmentRemoved attachmentRemoved = (AttachmentRemoved) domainEvent;
            AttachmentRemovedJsonDto dto = new AttachmentRemovedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(attachmentRemoved.workingCardId().id());
            dto.setAttachmentRemoved(attachmentRemoved.attachmentRemoved().id());
            dto.setAttachmentOrphaned(attachmentRemoved.attachmentOrphaned());
            dto.setOccurredOn(attachmentRemoved.occurredOn());

            return dto;
        }
        else if (domainEvent instanceof CardPublished) {
            CardPublished cardPublished = (CardPublished) domainEvent;
            CardPublishedJsonDto dto = new CardPublishedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(cardPublished.workingCardId().id());

            dto.setPriorPublishedImages(cardPublished.priorPublishedImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setCurrentPublishedImages(cardPublished.currentPublishedImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setPublishedImagesRemoved(cardPublished.publishedImagesRemoved().stream().map(ImageId::id).collect(Collectors.toSet()));
            dto.setPublishedImagesAdded(cardPublished.publishedImagesAdded().stream().map(ImageId::id).collect(Collectors.toSet()));

            dto.setPriorPublishedAttachments(cardPublished.priorPublishedAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setCurrentPublishedAttachments(cardPublished.currentPublishedAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setPublishedAttachmentsRemoved(cardPublished.publishedAttachmentsRemoved().stream().map(AttachmentId::id).collect(Collectors.toSet()));
            dto.setPublishedAttachmentsAdded(cardPublished.publishedAttachmentsAdded().stream().map(AttachmentId::id).collect(Collectors.toSet()));

            dto.setOccurredOn(cardPublished.occurredOn());

            return dto;
        }
        else if (domainEvent instanceof DraftDiscarded) {
            DraftDiscarded draftDiscarded = (DraftDiscarded) domainEvent;
            CardDraftDiscardedJsonDto dto = new CardDraftDiscardedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(draftDiscarded.workingCardId().id());

            dto.setPriorDraftImages(draftDiscarded.priorDraftImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setCurrentImages(draftDiscarded.currentImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setDraftImagesRemoved(draftDiscarded.draftImagesRemoved().stream().map(ImageId::id).collect(Collectors.toSet()));

            dto.setPriorDraftAttachments(draftDiscarded.priorDraftAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setCurrentAttachments(draftDiscarded.currentAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setDraftAttachmentsRemoved(draftDiscarded.draftAttachmentsRemoved().stream().map(AttachmentId::id).collect(Collectors.toSet()));

            dto.setOccurredOn(draftDiscarded.occurredOn());

            return dto;
        }
        else if (domainEvent instanceof CardDeleted) {
            CardDeleted cardDeleted = (CardDeleted) domainEvent;
            CardDeletedJsonDto dto = new CardDeletedJsonDto();

            dto.setId(storedEvent.id());
            dto.setWorkingCardId(cardDeleted.workingCardId().id());

            dto.setPublishedImages(cardDeleted.publishedImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setDraftImages(cardDeleted.draftImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setOrphanedImages(cardDeleted.orphanedImages().stream().map(ImageId::id).collect(Collectors.toSet()));

            dto.setPublishedAttachments(cardDeleted.publishedAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setDraftAttachments(cardDeleted.draftAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setOrphanedAttachments(cardDeleted.orphanedAttachments().stream().map(AttachmentId::id).collect(Collectors.toSet()));

            dto.setOccurredOn(cardDeleted.occurredOn());

            return dto;
        } else {
            throw new IllegalArgumentException("Error sending working card stored event via jms; " +
                    "working card domain event is an unknown type");
        }
    }
}
