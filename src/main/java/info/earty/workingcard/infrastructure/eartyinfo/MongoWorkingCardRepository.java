package info.earty.workingcard.infrastructure.eartyinfo;

import info.earty.infrastructure.mongo.MongoDocumentRepository;
import info.earty.workingcard.domain.model.card.WorkingCard;
import info.earty.workingcard.domain.model.workingpage.WorkingPageId;
import info.earty.workingcard.infrastructure.mongo.MongoWorkingCard;

import java.util.Set;

public interface MongoWorkingCardRepository extends MongoDocumentRepository<WorkingCard, MongoWorkingCard> {
    Set<MongoWorkingCard> findByWorkingPageId(WorkingPageId workingPageId);
}
