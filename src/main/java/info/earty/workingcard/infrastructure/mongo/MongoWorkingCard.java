package info.earty.workingcard.infrastructure.mongo;

import info.earty.infrastructure.mongo.Document;
import info.earty.workingcard.domain.model.card.DraftCard;
import info.earty.workingcard.domain.model.card.PublishedCard;
import info.earty.workingcard.domain.model.card.WorkingCard;
import info.earty.workingcard.domain.model.workingpage.WorkingPageId;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document("workingCard")
public class MongoWorkingCard extends Document<WorkingCard> {

    //Aggregate state
    private WorkingPageId workingPageId;
    private PublishedCard publishedCard;
    private DraftCard draftCard;
    private int nextDraftCardIdInt;
}
