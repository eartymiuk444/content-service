package info.earty.workingcard.infrastructure.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.infrastructure.autoconfigure.infrastructure.InfrastructureProperties;
import info.earty.sitemenu.infrastructure.jms.dto.DirectoryRemovedJsonDto;
import info.earty.sitemenu.infrastructure.jms.dto.PageAddedJsonDto;
import info.earty.sitemenu.infrastructure.jms.dto.PageRemovedJsonDto;
import info.earty.workingcard.application.WorkingCardCommandService;
import info.earty.workingcard.application.command.RemoveOnPageCommand;
import info.earty.workingcard.application.command.RemoveOnPagesCommand;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("workingCardSiteMenuMessageSubscriber")
@RequiredArgsConstructor
public class JmsSiteMenuMessageSubscriber {

    private final ObjectMapper objectMapper;

    private final WorkingCardCommandService workingCardCommandService;

    @JmsListener(destination = "info.earty.site-menu", subscription = "info.earty.site-menu.working-card")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(InfrastructureProperties.DEFAULT_JMS_TYPE_ID_PROPERTY);

        if (type.equals(PageAddedJsonDto.class.getName())) {
            //no-op
        }
        else if (type.equals(PageRemovedJsonDto.class.getName())) {
            PageRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PageRemovedJsonDto.class);

            RemoveOnPageCommand removeOnPageCommand = new RemoveOnPageCommand();
            removeOnPageCommand.setEventId(dto.getId());
            removeOnPageCommand.setSiteMenuId(dto.getSiteMenuId());

            removeOnPageCommand.setWorkingPageId(dto.getWorkingPageId());
            workingCardCommandService.removeCardsOnPage(removeOnPageCommand);
        }
        else if (type.equals(DirectoryRemovedJsonDto.class.getName())) {
            DirectoryRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), DirectoryRemovedJsonDto.class);

            RemoveOnPagesCommand removeOnPagesCommand = new RemoveOnPagesCommand();
            removeOnPagesCommand.setEventId(dto.getId());
            removeOnPagesCommand.setSiteMenuId(dto.getSiteMenuId());

            removeOnPagesCommand.setWorkingPageIds(dto.getPagesRemoved());
            workingCardCommandService.removeCardsOnPages(removeOnPagesCommand);
        }
        else {
            throw new IllegalArgumentException("Error receiving site-menu message; unknown message type");
        }
    }
}
