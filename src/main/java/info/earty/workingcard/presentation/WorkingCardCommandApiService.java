package info.earty.workingcard.presentation;

import info.earty.workingcard.application.WorkingCardCommandService;
import info.earty.workingcard.application.command.ChangeTextCommand;
import info.earty.workingcard.application.command.RemoveAttachmentCommand;
import info.earty.workingcard.application.command.RemoveImageCommand;
import info.earty.workingcard.presentation.command.ChangeTextJsonCommand;
import info.earty.workingcard.presentation.command.RemoveAttachmentJsonCommand;
import info.earty.workingcard.presentation.command.RemoveImageJsonCommand;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WorkingCardCommandApiService implements WorkingCardCommandApi {

    private final WorkingCardCommandService workingCardCommandService;
    private final JsonCommandMapper jsonCommandMapper;

    @Override
    public void changeText(ChangeTextJsonCommand jsonCommand) {
        workingCardCommandService.changeText(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removeImage(RemoveImageJsonCommand jsonCommand) {
        workingCardCommandService.removeImage(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removeAttachment(RemoveAttachmentJsonCommand jsonCommand) {
        workingCardCommandService.removeAttachment(jsonCommandMapper.map(jsonCommand));
    }

    @Mapper(componentModel = "spring")
    interface JsonCommandMapper {
        ChangeTextCommand map(ChangeTextJsonCommand jsonCommand);
        RemoveImageCommand map(RemoveImageJsonCommand jsonCommand);
        RemoveAttachmentCommand map(RemoveAttachmentJsonCommand jsonCommand);
    }
}
