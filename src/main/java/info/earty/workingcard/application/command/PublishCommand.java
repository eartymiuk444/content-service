package info.earty.workingcard.application.command;

import lombok.Data;

import java.util.Set;

@Data
public class PublishCommand {

    private int eventId;
    private String workingPageId;
    private Set<String> publishWorkingCardIds;
    private Set<String> removeWorkingCardIds;

}
