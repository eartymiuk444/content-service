package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class RemoveAttachmentCommand {

    private String workingCardId;
    private String attachmentId;

}
