package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class AddImageCommand {

    private String workingCardId;
    private String imageId;

}
