package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class ChangeTextCommand {
    private String workingCardId;
    private String text;
}
