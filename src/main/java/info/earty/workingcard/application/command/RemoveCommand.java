package info.earty.workingcard.application.command;

import lombok.Data;

import java.util.Set;

@Data
public class RemoveCommand {
    private int eventId;
    private String workingPageId;
    private Set<String> workingCardIds;
}
