package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class CreateCommand {

    private int eventId;
    private String workingCardId;
    private String workingPageId;
    private String title;

}
