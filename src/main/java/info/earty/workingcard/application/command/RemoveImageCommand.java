package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class RemoveImageCommand {

    private String workingCardId;
    private String imageId;

}
