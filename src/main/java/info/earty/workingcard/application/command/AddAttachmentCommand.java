package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class AddAttachmentCommand {

    private String workingCardId;
    private String attachmentId;
    private String attachmentFilename;

}
