package info.earty.workingcard.application.command;

import lombok.Data;

@Data
public class RemoveOnPageCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
}
