package info.earty.workingcard.application.command;

import lombok.Data;

import java.util.Set;

@Data
public class DiscardCommand {

    private int eventId;
    private String workingPageId;
    Set<String> discardWorkingCardIds;
    Set<String> removeWorkingCardIds;

}
