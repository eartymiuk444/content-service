package info.earty.workingcard.domain.model.sitemenu;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.PartnerAggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class SiteMenuId implements PartnerAggregateId {
    String id;
}
