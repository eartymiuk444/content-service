package info.earty.workingcard.domain.model.card;

import info.earty.domain.model.BasicRepository;
import info.earty.workingcard.domain.model.workingpage.WorkingPageId;

import java.util.Optional;
import java.util.Set;

public interface WorkingCardRepository extends BasicRepository<WorkingCard> {
    Set<WorkingCard> findByWorkingPageId(WorkingPageId workingPageId);
}
