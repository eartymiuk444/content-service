package info.earty.workingcard.domain.model.card;

import info.earty.domain.model.AggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class WorkingCardId implements AggregateId<WorkingCard> {
    String id;

    @Override
    public Class<WorkingCard> aggregateType() {
        return WorkingCard.class;
    }
}
