package info.earty.workingcard.domain.model.card;

public interface WorkingCardIdentityService {
    WorkingCardId generate();
}
