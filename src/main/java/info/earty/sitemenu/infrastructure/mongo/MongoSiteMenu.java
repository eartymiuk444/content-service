package info.earty.sitemenu.infrastructure.mongo;

import info.earty.infrastructure.mongo.Document;
import info.earty.sitemenu.domain.model.sitemenu.Directory;
import info.earty.sitemenu.domain.model.sitemenu.DirectoryId;
import info.earty.sitemenu.domain.model.sitemenu.DirectoryItem;
import info.earty.sitemenu.domain.model.sitemenu.SiteMenu;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document("siteMenu")
public class MongoSiteMenu extends Document<SiteMenu> {
    private DirectoryId rootDirectoryId;
    private Map<Integer, Directory> allDirectories;
    private Map<String, DirectoryItem> allPages;
    private Map<Integer, DirectoryId> directoryParents;
    private Map<String, DirectoryId> pageParents;
    private int nextDirectoryIdInt;
}
