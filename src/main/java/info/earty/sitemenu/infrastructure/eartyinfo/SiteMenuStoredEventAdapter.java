package info.earty.sitemenu.infrastructure.eartyinfo;

import info.earty.application.StoredEvent;
import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.infrastructure.jms.spring.StoredEventAdapter;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageId;
import info.earty.sitemenu.infrastructure.jms.dto.DirectoryRemovedJsonDto;
import info.earty.sitemenu.infrastructure.jms.dto.PageAddedJsonDto;
import info.earty.sitemenu.infrastructure.jms.dto.PageRemovedJsonDto;
import info.earty.sitemenu.domain.model.sitemenu.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class SiteMenuStoredEventAdapter implements StoredEventAdapter<SiteMenu> {

    @Override
    public String adaptAggregateIdToString(AggregateId<SiteMenu> aggregateId) {
        Assert.isTrue(aggregateId instanceof SiteMenuId, this.getClass().getSimpleName() + " : unexpected aggregate id type");
        SiteMenuId siteMenuId = (SiteMenuId) aggregateId;

        return siteMenuId.id();
    }

    @Override
    public Object adaptToMessageConvertibleObject(StoredEvent<SiteMenu> storedEvent) {
        DomainEvent<SiteMenu> domainEvent = storedEvent.domainEvent();

        if (domainEvent instanceof PageAdded) {
            PageAdded pageAdded = (PageAdded)domainEvent;
            PageAddedJsonDto dto = new PageAddedJsonDto();

            dto.setId(storedEvent.id());
            dto.setSiteMenuId(pageAdded.siteMenuId().id());
            dto.setOccurredOn(pageAdded.occurredOn());

            dto.setWorkingPageId(pageAdded.workingPageId().id());
            dto.setName(pageAdded.name().nameString());

            return dto;
        }
        else if (domainEvent instanceof PageRemoved) {
            PageRemoved pageRemoved = (PageRemoved) domainEvent;
            PageRemovedJsonDto dto = new PageRemovedJsonDto();

            dto.setId(storedEvent.id());
            dto.setSiteMenuId(pageRemoved.siteMenuId().id());
            dto.setOccurredOn(pageRemoved.occurredOn());

            dto.setWorkingPageId(pageRemoved.workingPageId().id());

            return dto;
        }
        else if (domainEvent instanceof DirectoryRemoved) {
            DirectoryRemoved directoryRemoved = (DirectoryRemoved) domainEvent;
            DirectoryRemovedJsonDto dto = new DirectoryRemovedJsonDto();

            dto.setId(storedEvent.id());
            dto.setSiteMenuId(directoryRemoved.siteMenuId().id());
            dto.setOccurredOn(directoryRemoved.occurredOn());

            dto.setPagesRemoved(directoryRemoved.pagesRemoved().stream().map(WorkingPageId::id)
                    .collect(Collectors.toSet()));

            return dto;
        }
        else {
            throw new IllegalArgumentException("Error sending working page stored event via jms; " +
                    "working page domain event is an unknown type");
        }
    }
}
