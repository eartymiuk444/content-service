package info.earty.sitemenu.infrastructure.eartyinfo;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.domain.model.PartnerAggregateId;
import info.earty.infrastructure.mongo.AggregateAdapter;
import info.earty.sitemenu.domain.model.sitemenu.*;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageId;
import info.earty.sitemenu.infrastructure.mongo.MongoSiteMenu;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

import static info.earty.infrastructure.reflection.ReflectionHelpers.getField;

@Component
public class SiteMenuAdapter extends AggregateAdapter<SiteMenu, MongoSiteMenu> {

    private final MappingMongoConverter mappingMongoConverter;

    public SiteMenuAdapter(MappingMongoConverter mappingMongoConverter) {
        super(SiteMenu.class, MongoSiteMenu.class);
        this.mappingMongoConverter = mappingMongoConverter;
    }

    @Override
    protected String adaptIdToString(AggregateId<SiteMenu> aggregateId) {
        Assert.isTrue(aggregateId instanceof SiteMenuId, this.getClass().getSimpleName() + " : unexpected aggregate id type");
        SiteMenuId siteMenuId = (SiteMenuId) aggregateId;

        return siteMenuId.id();
    }

    @Override
    protected String adaptPartnerIdToString(PartnerAggregateId partnerId) {
        Assert.isTrue(partnerId instanceof WorkingPageId, this.getClass().getSimpleName() + " : unexpected partner id type");
        WorkingPageId workingPageId = (WorkingPageId) partnerId;

        return workingPageId.id();
    }

    @Override
    protected org.bson.Document adaptDomainEventToBson(DomainEvent<SiteMenu> siteMenuDomainEvent) {
        org.bson.Document document = new org.bson.Document();
        mappingMongoConverter.write(siteMenuDomainEvent, document);
        return document;
    }

    @Override
    protected void clearAggregateInternal(MongoSiteMenu mongoSiteMenu) {
        mongoSiteMenu.setRootDirectoryId(null);
        mongoSiteMenu.setAllDirectories(null);
        mongoSiteMenu.setAllPages(null);
        mongoSiteMenu.setDirectoryParents(null);
        mongoSiteMenu.setPageParents(null);
        mongoSiteMenu.setNextDirectoryIdInt(0);
    }

    @Override
    protected void setAggregateInternal(SiteMenu aggregate, MongoSiteMenu mongoSiteMenu) {
        //domain provides read access
        mongoSiteMenu.setRootDirectoryId(aggregate.rootDirectory().id());

        //domain restricts read access
        //allDirectories
        Map<DirectoryId, Directory> allDirectoriesDomain = getField(aggregate, "allDirectories", Map.class);
        Map<Integer, Directory> allDirectories = new HashMap<>();
        allDirectoriesDomain.forEach((directoryId, directory) -> allDirectories.put(directoryId.id(), directory));
        mongoSiteMenu.setAllDirectories(allDirectories);

        //allPages
        Map<WorkingPageId, DirectoryItem> allPagesDomain = getField(aggregate, "allPages", Map.class);
        Map<String, DirectoryItem> allPages = new HashMap<>();
        allPagesDomain.forEach((workingPageId, directoryItem) -> allPages.put(workingPageId.id(), directoryItem));
        mongoSiteMenu.setAllPages(allPages);

        //directoryParents
        Map<DirectoryId, DirectoryId> directoryParentsDomain = getField(aggregate, "directoryParents", Map.class);
        Map<Integer, DirectoryId> directoryParents = new HashMap<>();
        directoryParentsDomain.forEach((directoryId, parentId) -> directoryParents.put(directoryId.id(), parentId));
        mongoSiteMenu.setDirectoryParents(directoryParents);

        //pageParents
        Map<WorkingPageId, DirectoryId> pageParentsDomain = getField(aggregate, "pageParents", Map.class);
        Map<String, DirectoryId> pageParents = new HashMap<>();
        pageParentsDomain.forEach((workingPageId, parentId) -> pageParents.put(workingPageId.id(), parentId));
        mongoSiteMenu.setPageParents(pageParents);

        //nextDirectoryIdInt
        mongoSiteMenu.setNextDirectoryIdInt(getField(aggregate, "nextDirectoryIdInt", Integer.class));
    }

    @Override
    protected MongoSiteMenu newDocumentInstance() {
        return new MongoSiteMenu();
    }
}