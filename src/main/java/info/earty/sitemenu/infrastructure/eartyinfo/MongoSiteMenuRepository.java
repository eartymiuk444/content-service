package info.earty.sitemenu.infrastructure.eartyinfo;

import info.earty.infrastructure.mongo.MongoDocumentRepository;
import info.earty.sitemenu.domain.model.sitemenu.SiteMenu;
import info.earty.sitemenu.infrastructure.mongo.MongoSiteMenu;

public interface MongoSiteMenuRepository extends MongoDocumentRepository<SiteMenu, MongoSiteMenu> {
}
