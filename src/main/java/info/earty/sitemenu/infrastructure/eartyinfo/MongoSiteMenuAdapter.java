package info.earty.sitemenu.infrastructure.eartyinfo;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.domain.model.PartnerAggregateId;
import info.earty.infrastructure.mongo.MongoDocumentAdapter;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageId;
import info.earty.sitemenu.domain.model.sitemenu.*;
import info.earty.sitemenu.infrastructure.mongo.MongoSiteMenu;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

import static info.earty.infrastructure.reflection.ReflectionHelpers.*;

@Component
public class MongoSiteMenuAdapter extends MongoDocumentAdapter<MongoSiteMenu, SiteMenu> {

    private final MappingMongoConverter mappingMongoConverter;

    public MongoSiteMenuAdapter(MappingMongoConverter mappingMongoConverter) {
        super(MongoSiteMenu.class, SiteMenu.class);
        this.mappingMongoConverter = mappingMongoConverter;
    }

    @Override
    public SiteMenu reconstituteAggregate(MongoSiteMenu mongoSiteMenu) {
        Assert.notNull(mongoSiteMenu, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(mongoSiteMenu.isHasAggregate(), "Error reconstituting aggregate; the document does not contain an aggregate instance");

        Map<DirectoryId, Directory> allDirectories = new HashMap<>();
        mongoSiteMenu.getAllDirectories().forEach((directoryId, directory) -> allDirectories.put(new DirectoryId(directoryId), directory));

        Map<WorkingPageId, DirectoryItem> allPages = new HashMap<>();
        mongoSiteMenu.getAllPages().forEach((workingPageId, page) -> allPages.put(new WorkingPageId(workingPageId), page));

        Map<DirectoryId, DirectoryId> directoryParents = new HashMap<>();
        mongoSiteMenu.getDirectoryParents().forEach((directoryId, parentId) -> directoryParents.put(new DirectoryId(directoryId), parentId));

        Map<WorkingPageId, DirectoryId> pageParents = new HashMap<>();
        mongoSiteMenu.getPageParents().forEach((workingPageId, parentId) -> pageParents.put(new WorkingPageId(workingPageId), parentId));

        SiteMenu siteMenu = newInstance(accessibleConstructor(SiteMenu.class, SiteMenuId.class, DirectoryId.class, Map.class, Map.class, Map.class, Map.class),
                new SiteMenuId(mongoSiteMenu.getId()), mongoSiteMenu.getRootDirectoryId(), allDirectories, allPages, directoryParents, pageParents);

        setField(siteMenu, "nextDirectoryIdInt", mongoSiteMenu.getNextDirectoryIdInt());

        return siteMenu;
    }

    @Override
    protected AggregateId<SiteMenu> reconstituteAggregateId(String documentKey) {
        Assert.notNull(documentKey, this.getClass().getSimpleName() + " : document key cannot be null");
        return new SiteMenuId(documentKey);
    }

    @Override
    protected PartnerAggregateId reconstitutePartnerId(Class<? extends PartnerAggregateId> partnerIdType, String partnerKey) {
        Assert.notNull(partnerIdType, this.getClass().getSimpleName() + " : partner id type cannot be null");
        Assert.notNull(partnerKey, this.getClass().getSimpleName() + " : partner key cannot be null");

        Assert.isTrue(partnerIdType.equals(WorkingPageId.class), this.getClass().getSimpleName() + " : unexpected partner id type");
        return new WorkingPageId(partnerKey);
    }

    @Override
    protected DomainEvent<SiteMenu> reconstituteDomainEvent(org.bson.Document bsonDomainEvent) {
        return (DomainEvent<SiteMenu>) mappingMongoConverter.read(
                mappingMongoConverter.getTypeMapper().readType(bsonDomainEvent).getType(), bsonDomainEvent);
    }
}
