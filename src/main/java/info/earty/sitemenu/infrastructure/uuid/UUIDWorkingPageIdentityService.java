package info.earty.sitemenu.infrastructure.uuid;

import info.earty.sitemenu.domain.model.workingpage.WorkingPageId;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageIdentityService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDWorkingPageIdentityService implements WorkingPageIdentityService {
    @Override
    public WorkingPageId generate() {
        return new WorkingPageId(UUID.randomUUID().toString());
    }
}
