package info.earty.sitemenu.infrastructure.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.infrastructure.autoconfigure.infrastructure.InfrastructureProperties;
import info.earty.sitemenu.application.SiteMenuCommandService;
import info.earty.sitemenu.application.command.ChangePageNameCommand;
import info.earty.workingpage.infrastructure.jms.dto.PagePublishedJsonDto;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("siteMenuWorkingPageMessageSubscriber")
@RequiredArgsConstructor
public class JmsWorkingPageMessageSubscriber {

    private final ObjectMapper objectMapper;

    private final SiteMenuCommandService siteMenuCommandService;

    @JmsListener(destination = "info.earty.working-page", subscription = "info.earty.working-page.site-menu")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(InfrastructureProperties.DEFAULT_JMS_TYPE_ID_PROPERTY);

        if (type.equals(PagePublishedJsonDto.class.getName())) {
            PagePublishedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PagePublishedJsonDto.class);

            ChangePageNameCommand changePageNameCommand = new ChangePageNameCommand();
            changePageNameCommand.setEventId(dto.getId());
            changePageNameCommand.setSiteMenuId(dto.getSiteMenuId());
            changePageNameCommand.setWorkingPageId(dto.getWorkingPageId());

            changePageNameCommand.setPageName(dto.getPublishedTitle());
            siteMenuCommandService.changePageName(changePageNameCommand);
        }
    }

}
