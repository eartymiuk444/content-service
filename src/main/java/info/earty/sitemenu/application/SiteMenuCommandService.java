package info.earty.sitemenu.application;
import info.earty.application.*;
import info.earty.application.aspect.UnitOfWork;
import info.earty.domain.model.BasicRepository;
import info.earty.sitemenu.application.command.*;
import info.earty.sitemenu.domain.model.sitemenu.*;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageId;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageIdentityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@UnitOfWork(outboxEnqueuers = SiteMenu.class)
public class SiteMenuCommandService {

    private final SiteMenuFactory siteMenuFactory;
    private final SiteMenuIdentityService siteMenuIdentityService;
    private final BasicRepository<SiteMenu> siteMenuRepository;

    private final OutboxRepository<SiteMenu> siteMenuOutboxRepository;
    private final InboxRepository<SiteMenu> siteMenuInboxRepository;

    private final WorkingPageIdentityService workingPageIdentityService;

    public void create() {
        SiteMenuId siteMenuId = siteMenuIdentityService.eartyInfoId();
        siteMenuOutboxRepository.add(new Outbox<>(siteMenuId));
        siteMenuInboxRepository.add(new Inbox<>(siteMenuId));
        siteMenuRepository.add(siteMenuFactory.create(siteMenuId));
    }

    public void addDirectory(AddDirectoryCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error adding directory; site menu with id not found"));

        siteMenu.addDirectory(DirectoryItemName.create(aCommand.getName()), new PathSegment(aCommand.getPathSegment()),
                new DirectoryId(aCommand.getParentDirectoryId()));
    }

    public void moveDirectoryUp(MoveDirectoryUpCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving directory up; site menu with id not found"));
        Directory parentDirectory = siteMenu.findDirectory(new DirectoryId(aCommand.getParentDirectoryId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving directory up; parent directory not found"));

        parentDirectory.moveSubDirectoryUp(new DirectoryId(aCommand.getDirectoryId()));
    }

    public void moveDirectoryDown(MoveDirectoryDownCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving directory down; site menu with id not found"));
        Directory parentDirectory = siteMenu.findDirectory(new DirectoryId(aCommand.getParentDirectoryId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving directory down; parent directory not found"));

        parentDirectory.moveSubDirectoryDown(new DirectoryId(aCommand.getDirectoryId()));
    }

    public void moveDirectory(MoveDirectoryCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving directory; site menu with id not found"));

        siteMenu.moveDirectory(new DirectoryId(aCommand.getDirectoryId()), new DirectoryId(aCommand.getParentDirectoryId()));
    }

    public void addPage(AddPageCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error adding page; site menu with id not found"));

        WorkingPageId workingPageId = workingPageIdentityService.generate();

        siteMenu.addPage(workingPageId, DirectoryItemName.create(aCommand.getTitle()), new PathSegment(aCommand.getPathSegment()),
                new DirectoryId(aCommand.getParentDirectoryId()));
    }

    public void movePageUp(MovePageUpCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page up; site menu with id not found"));
        Directory parentDirectory = siteMenu.findDirectory(new DirectoryId(aCommand.getParentDirectoryId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page up; parent directory not found"));

        parentDirectory.movePageUp(new WorkingPageId(aCommand.getWorkingPageId()));
    }

    public void movePageDown(MovePageDownCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page down; site menu with id not found"));
        Directory parentDirectory = siteMenu.findDirectory(new DirectoryId(aCommand.getParentDirectoryId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page down; parent directory not found"));

        parentDirectory.movePageDown(new WorkingPageId(aCommand.getWorkingPageId()));
    }

    public void movePage(MovePageCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page; site menu with id not found"));

        siteMenu.movePage(new WorkingPageId(aCommand.getWorkingPageId()), new DirectoryId(aCommand.getParentDirectoryId()));
    }

    public void removeDirectory(RemoveDirectoryCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page; site menu with id not found"));

        siteMenu.removeDirectory(new DirectoryId(aCommand.getDirectoryId()));
    }

    public void removePage(RemovePageCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error moving page; site menu with id not found"));
        siteMenu.removePage(new WorkingPageId(aCommand.getWorkingPageId()));
    }

    public void editDirectory(EditDirectoryCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error editing directory; site menu with id not found"));

        Directory directory = siteMenu.findDirectory(new DirectoryId(aCommand.getDirectoryId()))
                .orElseThrow(() -> new IllegalArgumentException("Error editing directory; directory with id not found"));

        if (!directory.name().nameString().equals(aCommand.getName())) {
            siteMenu.changeDirectoryName(directory.id(), DirectoryItemName.create(aCommand.getName()));
        }

        PathSegment pathSegment = new PathSegment(aCommand.getPathSegment());
        if (!directory.pathSegment().equals(pathSegment)) {
            siteMenu.changeDirectoryPathSegment(directory.id(), pathSegment);
        }
    }

    public void changePagePathSegment(ChangePagePathSegmentCommand aCommand) {
        SiteMenu siteMenu = siteMenuRepository.findById(new SiteMenuId(aCommand.getSiteMenuId()))
                .orElseThrow(() -> new IllegalArgumentException("Error editing page; site menu with id not found"));

        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        DirectoryItem page = siteMenu.findPage(workingPageId)
                .orElseThrow(() -> new IllegalArgumentException("Error editing page; page with id not found on site menu"));

        PathSegment pathSegment = new PathSegment(aCommand.getPathSegment());
        if (!page.pathSegment().equals(pathSegment)) {
            siteMenu.changePagePathSegment(workingPageId, pathSegment);
        }
    }

    public void changePageName(ChangePageNameCommand aCommand) { //event command
        SiteMenuId siteMenuId = new SiteMenuId(aCommand.getSiteMenuId());

        SiteMenu siteMenu = siteMenuRepository.findById(siteMenuId)
                .orElseThrow(() -> new IllegalArgumentException("Error changing page name; site menu with id not found"));

        Inbox<SiteMenu> siteMenuInbox = siteMenuInboxRepository.findByAggregateId(siteMenuId).get();
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        if (!siteMenuInbox.duplicateEvent(workingPageId, aCommand.getEventId())) { //deduplication
            DirectoryItem page = siteMenu.findPage(workingPageId)
                    .orElseThrow(() -> new IllegalArgumentException("Error changing page name; page with id not found on site menu"));

            if (!page.name().nameString().equals(aCommand.getPageName())) {
                siteMenu.changePageName(workingPageId, DirectoryItemName.create(aCommand.getPageName()));
            }
            siteMenuInbox.eventProcessed(workingPageId, aCommand.getEventId());
        }
    }

}
