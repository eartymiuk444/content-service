package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class MovePageDownCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
