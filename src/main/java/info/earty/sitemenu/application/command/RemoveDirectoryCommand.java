package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class RemoveDirectoryCommand {

    private String siteMenuId;
    private Integer directoryId;

}
