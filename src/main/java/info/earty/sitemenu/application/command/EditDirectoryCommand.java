package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class EditDirectoryCommand {
    private String siteMenuId;
    private Integer directoryId;
    private String name;
    private String pathSegment;
}
