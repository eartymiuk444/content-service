package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class RemovePageCommand {

    private String siteMenuId;
    private String workingPageId;

}
