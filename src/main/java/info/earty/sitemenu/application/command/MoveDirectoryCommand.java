package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class MoveDirectoryCommand {

    private String siteMenuId;
    private Integer directoryId;
    private Integer parentDirectoryId;

}
