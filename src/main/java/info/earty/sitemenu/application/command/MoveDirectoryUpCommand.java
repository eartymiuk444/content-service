package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class MoveDirectoryUpCommand {

    private String siteMenuId;
    private Integer directoryId;
    private Integer parentDirectoryId;

}
