package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class AddPageCommand {
    private String siteMenuId;
    private String title;
    private Integer parentDirectoryId;
    private String pathSegment;
}
