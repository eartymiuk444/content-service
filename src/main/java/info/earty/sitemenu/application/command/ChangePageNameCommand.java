package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class ChangePageNameCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
    private String pageName;
}
