package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class ChangePagePathSegmentCommand {
    private String siteMenuId;
    private String workingPageId;
    private String pathSegment;
}
