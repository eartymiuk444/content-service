package info.earty.sitemenu.application.command;

import lombok.Data;

@Data
public class MovePageUpCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
