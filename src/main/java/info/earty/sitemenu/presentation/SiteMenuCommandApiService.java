package info.earty.sitemenu.presentation;

import info.earty.sitemenu.application.SiteMenuCommandService;
import info.earty.sitemenu.application.command.*;
import info.earty.sitemenu.presentation.command.*;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SiteMenuCommandApiService implements SiteMenuCommandApi {

    private final SiteMenuCommandService siteMenuCommandService;
    private final JsonCommandMapper jsonCommandMapper;

    @Override
    public void create() {
        siteMenuCommandService.create();
    }

    @Override
    public void addDirectory(AddDirectoryJsonCommand aCommand) {
        siteMenuCommandService.addDirectory(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void moveDirectoryUp(MoveDirectoryUpJsonCommand aCommand) {
        siteMenuCommandService.moveDirectoryUp(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void moveDirectoryDown(MoveDirectoryDownJsonCommand aCommand) {
        siteMenuCommandService.moveDirectoryDown(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void moveDirectory(MoveDirectoryJsonCommand aCommand) {
        siteMenuCommandService.moveDirectory(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void addPage(AddPageJsonCommand aCommand) {
        siteMenuCommandService.addPage(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void movePageUp(MovePageUpJsonCommand aCommand) {
        siteMenuCommandService.movePageUp(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void movePageDown(MovePageDownJsonCommand aCommand) {
        siteMenuCommandService.movePageDown(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void movePage(MovePageJsonCommand aCommand) {
        siteMenuCommandService.movePage(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void removeDirectory(RemoveDirectoryJsonCommand aCommand) {
        siteMenuCommandService.removeDirectory(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void removePage(RemovePageJsonCommand aCommand) {
        siteMenuCommandService.removePage(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void editDirectory(EditDirectoryJsonCommand aCommand) {
        siteMenuCommandService.editDirectory(jsonCommandMapper.map(aCommand));
    }

    @Override
    public void changePagePathSegment(ChangePagePathSegmentJsonCommand aCommand) {
        siteMenuCommandService.changePagePathSegment(jsonCommandMapper.map(aCommand));
    }

//    @Override
//    public void changePageName(ChangePageNameJsonCommand aCommand) { //event command
//        siteMenuCommandService.changePageName(aCommand);
//    }

    @Mapper
    interface JsonCommandMapper {
        AddDirectoryCommand map(AddDirectoryJsonCommand jsonCommand);
        MoveDirectoryUpCommand map(MoveDirectoryUpJsonCommand jsonCommand);
        MoveDirectoryDownCommand map(MoveDirectoryDownJsonCommand jsonCommand);
        MoveDirectoryCommand map(MoveDirectoryJsonCommand jsonCommand);
        AddPageCommand map(AddPageJsonCommand jsonCommand);
        MovePageUpCommand map(MovePageUpJsonCommand jsonCommand);
        MovePageDownCommand map(MovePageDownJsonCommand jsonCommand);
        MovePageCommand map(MovePageJsonCommand jsonCommand);
        RemoveDirectoryCommand map(RemoveDirectoryJsonCommand jsonCommand);
        RemovePageCommand map(RemovePageJsonCommand jsonCommand);
        EditDirectoryCommand map(EditDirectoryJsonCommand jsonCommand);
        ChangePagePathSegmentCommand map(ChangePagePathSegmentJsonCommand jsonCommand);
    }

}
