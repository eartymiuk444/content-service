package info.earty.sitemenu.domain.model.sitemenu;

import info.earty.domain.model.AggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class SiteMenuId implements AggregateId<SiteMenu> {
    String id;

    @Override
    public Class<SiteMenu> aggregateType() {
        return SiteMenu.class;
    }
}
