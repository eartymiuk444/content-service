package info.earty.sitemenu.domain.model.sitemenu;

import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class DirectoryId {
    int id;
}
