package info.earty.sitemenu.domain.model.workingpage;

import info.earty.domain.model.PartnerAggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class WorkingPageId implements PartnerAggregateId {
    String id;
}
