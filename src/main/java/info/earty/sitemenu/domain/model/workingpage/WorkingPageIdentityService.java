package info.earty.sitemenu.domain.model.workingpage;

public interface WorkingPageIdentityService {
    WorkingPageId generate();
}
