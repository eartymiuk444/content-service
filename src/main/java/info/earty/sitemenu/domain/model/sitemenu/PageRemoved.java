package info.earty.sitemenu.domain.model.sitemenu;

import info.earty.domain.model.AggregateId;
import info.earty.domain.model.DomainEvent;
import info.earty.sitemenu.domain.model.workingpage.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PageRemoved implements DomainEvent<SiteMenu> {

    SiteMenuId siteMenuId;
    WorkingPageId workingPageId;
    Instant occurredOn;

    static PageRemoved create(SiteMenuId siteMenuId, WorkingPageId workingPageId) {
        Assert.notNull(siteMenuId, PageRemoved.class.getSimpleName() + ": site menu id cannot be null");
        Assert.notNull(workingPageId, PageRemoved.class.getSimpleName() + ": working page id cannot be null");

        return new PageRemoved(siteMenuId, workingPageId, Instant.now());
    }

    @Override
    public AggregateId<SiteMenu> aggregateId() {
        return this.siteMenuId();
    }

    @Override
    public Class<SiteMenu> aggregateType() {
        return SiteMenu.class;
    }
}
