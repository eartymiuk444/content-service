package info.earty.image.application;

import lombok.Data;

import java.util.Set;

@Data
public class DeleteCommand {

    private Set<String> imageIds;

}
