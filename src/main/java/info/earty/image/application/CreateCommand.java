package info.earty.image.application;

import lombok.Data;

import java.io.InputStream;

@Data
public class CreateCommand {

    private String workingCardId;
    private String filename;
    private String contentType;
    private InputStream inputStream;

}
