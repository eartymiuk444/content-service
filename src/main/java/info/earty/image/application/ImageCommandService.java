package info.earty.image.application;

import info.earty.image.domain.model.image.*;
import info.earty.image.domain.model.workingcard.WorkingCardId;
import info.earty.image.domain.model.workingcard.CardImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ImageCommandService {

    private final ImageIdentityService imageIdentityService;
    private final ImageFactory imageFactory;
    private final ImageRepository imageRepository;
    private final ImageUnitOfWorkService imageUnitOfWorkService;
    private final CardImageService cardImageService;

    public void create(CreateCommand aCommand) {
        Assert.notNull(cardImageService.cardImageFrom(WorkingCardId.create(aCommand.getWorkingCardId())),
                this.getClass().getSimpleName() + " : no working card found with id");
        Image image = imageFactory.create(imageIdentityService.generate(), WorkingCardId.create(aCommand.getWorkingCardId()),
                aCommand.getFilename(), aCommand.getContentType(), aCommand.getInputStream());
        imageRepository.add(image);
    }

    public void delete(DeleteCommand aCommand) { //event command
        for (String imageId : aCommand.getImageIds()) {
            imageRepository.remove(ImageId.create(imageId));
        }
    }

    public void publish(PublishCommand aCommand) { //event command
        for (String imageId : aCommand.getImageIds()) {

            Optional<Image> optionalImage = imageRepository.findById(ImageId.create(imageId));
            if (optionalImage.isPresent()) {
                Image image = optionalImage.get();
                image.publish();
                imageUnitOfWorkService.saveImageAsPublished(image);
            }
        }
    }

}
