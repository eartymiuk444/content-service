package info.earty.image.presentation;

import info.earty.image.application.ImageQueryService;
import info.earty.image.domain.model.image.Image;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Service
@RequiredArgsConstructor
public class ImageQueryApiService implements ImageQueryApi {

    private final ImageQueryService imageQueryService;

    private static final CacheControl CACHE_CONTROL;
    static {
        CACHE_CONTROL = new CacheControl();
        CACHE_CONTROL.setMaxAge((int) Duration.of(30*6, ChronoUnit.DAYS).toSeconds());
    }

    @Override
    public Response get(String imageId) {
        Image image = imageQueryService.get(imageId);
        return Response.ok(image.inputStream())
                .cacheControl(CACHE_CONTROL)
                .header(HttpHeaders.CONTENT_TYPE, image.contentType())
                .build();
    }

    @Override
    public Response getPublished(String imageId, String workingCardId) {
        Image image = imageQueryService.getPublished(imageId, workingCardId);
        return Response.ok(image.inputStream())
                .cacheControl(CACHE_CONTROL)
                .header(HttpHeaders.CONTENT_TYPE, image.contentType())
                .build();
    }
}
