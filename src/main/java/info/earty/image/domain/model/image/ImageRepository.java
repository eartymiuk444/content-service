package info.earty.image.domain.model.image;

import java.util.Optional;

public interface ImageRepository {
    void add(Image image);
    Optional<Image> findById(ImageId imageId);
    void remove(ImageId imageId);
}
