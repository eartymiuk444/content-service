package info.earty.image.domain.model.image;

import info.earty.image.domain.model.workingcard.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.io.InputStream;

@RequiredArgsConstructor(access= AccessLevel.PACKAGE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Image {

    @EqualsAndHashCode.Include
    private final ImageId id;
    private WorkingCardId workingCardId;

    private String filename;
    private String contentType;
    private InputStream inputStream;

    private boolean published;

    public ImageId id() {
        return this.id;
    }

    public WorkingCardId workingCardId() {
        return this.workingCardId;
    }

    public String filename() {
        return this.filename;
    }

    public String contentType() {
        return this.contentType;
    }

    public InputStream inputStream() {
        return this.inputStream;
    }

    public boolean published() {
        return this.published;
    }

    public void publish() {
        this.setPublished(true);
    }

    void setWorkingCardId(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, this.getClass().getSimpleName() + ": working card id cannot be null");
        this.workingCardId = workingCardId;
    }

    void setFilename(String filename) {
        Assert.notNull(filename, this.getClass().getSimpleName() + ": filename cannot be null");
        this.filename = filename;
    }

    void setContentType(String contentType) {
        Assert.notNull(contentType, this.getClass().getSimpleName() + ": content type cannot be null");
        this.contentType = contentType;
    }

    void setInputStream(InputStream inputStream) {
        Assert.notNull(inputStream, this.getClass().getSimpleName() + ": input stream cannot be null");
        this.inputStream = inputStream;
    }

    void setPublished(boolean published) {
        this.published = published;
    }

}
