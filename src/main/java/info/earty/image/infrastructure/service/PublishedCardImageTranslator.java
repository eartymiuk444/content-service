package info.earty.image.infrastructure.service;

import info.earty.workingcard.presentation.data.PublishedCardJsonDto;
import info.earty.image.domain.model.workingcard.PublishedCardImage;
import org.springframework.stereotype.Component;

@Component
public class PublishedCardImageTranslator {

    public PublishedCardImage toPublishedCardImageFromRepresentation(String workingCardId, PublishedCardJsonDto.Image publishedCardJsonDtoImage) {
        return PublishedCardImage.create(workingCardId, publishedCardJsonDtoImage.getImageId());
    }


}
