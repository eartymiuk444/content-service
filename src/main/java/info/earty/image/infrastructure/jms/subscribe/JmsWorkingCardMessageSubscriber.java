package info.earty.image.infrastructure.jms.subscribe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.image.application.DeleteCommand;
import info.earty.image.application.ImageCommandService;
import info.earty.image.application.PublishCommand;
import info.earty.infrastructure.autoconfigure.infrastructure.InfrastructureProperties;
import info.earty.workingcard.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component("imageWorkingCardMessageSubscriber")
@RequiredArgsConstructor
public class JmsWorkingCardMessageSubscriber {

    private final ObjectMapper objectMapper;

    private final ImageCommandService imageCommandService;

    @JmsListener(destination = "info.earty.working-card", subscription = "info.earty.working-card.image")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(InfrastructureProperties.DEFAULT_JMS_TYPE_ID_PROPERTY);

        if (type.equals(AddImageFailedJsonDto.class.getName())) {
            AddImageFailedJsonDto dto = objectMapper.readValue(message.getBody(String.class), AddImageFailedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            Set<String> imageIds = new HashSet<>();
            imageIds.add(dto.getImageId());
            deleteCommand.setImageIds(imageIds);
            imageCommandService.delete(deleteCommand);

        }
        else if (type.equals(ImageRemovedJsonDto.class.getName())) {
            ImageRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), ImageRemovedJsonDto.class);
            if (dto.isImageOrphaned()) {
                DeleteCommand deleteCommand = new DeleteCommand();
                Set<String> imageIds = new HashSet<>();
                imageIds.add(dto.getImageRemoved());
                deleteCommand.setImageIds(imageIds);
                imageCommandService.delete(deleteCommand);
            }
        }
        else if (type.equals(CardPublishedJsonDto.class.getName())) {
            CardPublishedJsonDto dto = objectMapper.readValue(message.getBody(String.class), CardPublishedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            deleteCommand.setImageIds(new HashSet<>(dto.getPublishedImagesRemoved()));
            imageCommandService.delete(deleteCommand);

            PublishCommand publishCommand = new PublishCommand();
            publishCommand.setImageIds(new HashSet<>(dto.getPublishedImagesAdded()));
            imageCommandService.publish(publishCommand);

        }
        else if (type.equals(CardDraftDiscardedJsonDto.class.getName())) {
            CardDraftDiscardedJsonDto dto = objectMapper.readValue(message.getBody(String.class), CardDraftDiscardedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            deleteCommand.setImageIds(new HashSet<>(dto.getDraftImagesRemoved()));
            imageCommandService.delete(deleteCommand);

        }
        else if (type.equals(CardDeletedJsonDto.class.getName())) {
            CardDeletedJsonDto dto = objectMapper.readValue(message.getBody(String.class), CardDeletedJsonDto.class);

            DeleteCommand deleteCommand = new DeleteCommand();
            deleteCommand.setImageIds(new HashSet<>(dto.getOrphanedImages()));
            imageCommandService.delete(deleteCommand);

        }
        else if (type.equals(AttachmentRemovedJsonDto.class.getName())) {
            //no-op
        }
        else if (type.equals(AddAttachmentFailedJsonDto.class.getName())) {
            //no-op
        }
        else {
            throw new IllegalArgumentException("Error receiving working-card message; unknown message type");
        }
    }

}
