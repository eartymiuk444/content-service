package info.earty.image;

import info.earty.ContentApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ContentApplication.class)
class ImageApplicationTests {

    @Test
    void contextLoads() {
    }

}
