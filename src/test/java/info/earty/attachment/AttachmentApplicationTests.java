package info.earty.attachment;

import info.earty.ContentApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ContentApplication.class)
class AttachmentApplicationTests {

    @Test
    void contextLoads() {
    }

}
