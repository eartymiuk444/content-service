package info.earty.workingpage.domain.model

import info.earty.domain.model.DomainEvent
import info.earty.domain.model.DomainEventPublisher
import info.earty.domain.model.DomainEventSubscriber
import info.earty.domain.model.EventTypeSubscriber

class DomainEventPublisherHelper {

    def static subscribeAndCaptureAll() {
        List<DomainEvent> domainEvents = new ArrayList<>()
        DomainEventPublisher.instance().reset()
        DomainEventPublisher.instance().subscribe(new EventTypeSubscriber<DomainEvent>() {
            @Override
            void handleEvent(DomainEvent aDomainEvent) {
                domainEvents.add(aDomainEvent)
            }
            @Override
            Class<DomainEvent> subscribedToEventType() {
                return DomainEvent.class
            }
        })
        return domainEvents
    }

}
