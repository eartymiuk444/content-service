package info.earty.workingpage.domain.model.workingpage

import info.earty.domain.model.DomainEvent
import info.earty.workingpage.domain.model.DomainEventPublisherHelper
import info.earty.workingpage.domain.model.sitemenu.SiteMenuId
import info.earty.workingpage.domain.model.workingcard.WorkingCardId
import spock.lang.Specification

class DraftSpec extends Specification {

    def workingPageFactoryCollaborator = new WorkingPageFactory()

    def "add an outline item to an empty working page draft"() {
        given: "a working page without any items and a working card"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))

        when: "the card is added as an outline item to the working page's draft"
        workingPage.addOutlineItemToRoot(new WorkingCardId("1"), "Working Card")

        then: "the working page has a single root outline item corresponding to the card added"
        workingPage.draft().rootOutlineItem().subItems().size() == 1
        DraftOutlineSubItem draftOutlineSubItem = workingPage.draft().rootOutlineItem().subItems().iterator().next()
        draftOutlineSubItem.title() == "Working Card"
        !draftOutlineSubItem.hasFragment()
    }

    def "try to add an outline item to a working page draft that already contains the working card"() {
        given: "a working page with an outline item corresponding to a working card"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        workingPage.addOutlineItemToRoot(new WorkingCardId("1"), "Working Card")

        when: "trying to add the same card as an outline item"
        workingPage.addOutlineItemToRoot(new WorkingCardId("1"), "Working Card 2")

        then: "an error occurs"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error adding outline item to page draft; draft already contains a card with id"
    }

    def "change the title of an outline item that is also a sub-item"() {
        given: "a working page with a root item that has a sub-item on its draft"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))

        WorkingCardId parentCardId = new WorkingCardId("1")
        String parentCardTitle = "Parent Card"
        workingPage.addOutlineItemToRoot(parentCardId, parentCardTitle)

        WorkingCardId childCardId = new WorkingCardId("2")
        String childCardTitle = "Child Card"
        workingPage.addOutlineItemToRoot(childCardId, childCardTitle)

        workingPage.draft().moveOutlineItemToParent(childCardId, parentCardId)
        String childCardChangedTitle = "Child Card Updated Title"

        and: "a domain event publisher and generic subscriber"
        List<DomainEvent> domainEvents = DomainEventPublisherHelper.subscribeAndCaptureAll()

        when: "the outline item that is also a sub-item has its title changed"
        workingPage.changeOutlineItemTitle(childCardId, childCardChangedTitle)

        then: "the outline item's title is changed and the corresponding sub-item on the parent outline item has its title changed"
        DraftOutlineItem childOutlineItem = workingPage.draft().findOutlineItem(childCardId).get()
        DraftOutlineItem parentOutlineItem = workingPage.draft().findOutlineItem(parentCardId).get()
        childOutlineItem.title() == childCardChangedTitle
        parentOutlineItem.subItems().get(0).title() == childCardChangedTitle

        and: "a DraftOutlineItemTitleChanged domain event is published with the expected working card id and title"
        domainEvents.size() == 1
        domainEvents.get(0).getClass() == DraftOutlineItemTitleChanged.class
        DraftOutlineItemTitleChanged draftOutlineItemTitleChanged = (DraftOutlineItemTitleChanged)domainEvents.get(0)
        draftOutlineItemTitleChanged.workingPageId() == workingPage.id()
        draftOutlineItemTitleChanged.workingCardId() == childCardId
        draftOutlineItemTitleChanged.title() == childCardChangedTitle
    }

    def "change the title of a root outline item"() {
        given: "a working page with a single root item"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))

        WorkingCardId cardId = new WorkingCardId("1")
        String cardTitle = "Card"
        workingPage.addOutlineItemToRoot(cardId, cardTitle)

        String updatedTitle = "Card New Title"

        and: "a domain event publisher and generic subscriber"
        List<DomainEvent> domainEvents = DomainEventPublisherHelper.subscribeAndCaptureAll()

        when: "the outline item has its title changed"
        workingPage.changeOutlineItemTitle(cardId, updatedTitle)

        then: "the outline item and the corresponding sub-item on the root have the updated title"
        DraftOutlineItem rootOutlineItem = workingPage.draft().findOutlineItem(cardId).get()
        rootOutlineItem.title() == updatedTitle
        workingPage.draft().rootOutlineItem().subItems().get(0).title() == updatedTitle

        and: "a DraftOutlineItemTitleChanged domain event is published with the expected working card id and title"
        domainEvents.size() == 1
        domainEvents.get(0).getClass() == DraftOutlineItemTitleChanged.class
        DraftOutlineItemTitleChanged draftOutlineItemTitleChanged = (DraftOutlineItemTitleChanged)domainEvents.get(0)
        draftOutlineItemTitleChanged.workingPageId() == workingPage.id()
        draftOutlineItemTitleChanged.workingCardId() == cardId
        draftOutlineItemTitleChanged.title() == updatedTitle
    }

    def "add a fragment to an outline item that is also a sub-item"() {
        given: "a working page with a root item that has a sub-item without a fragment on its draft"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        WorkingCardId parentCardId = new WorkingCardId("1")
        String parentCardTitle = "Parent Card"
        workingPage.addOutlineItemToRoot(parentCardId, parentCardTitle)

        WorkingCardId childCardId = new WorkingCardId("2")
        String childCardTitle = "Child Card"
        workingPage.addOutlineItemToRoot(childCardId, childCardTitle)
        workingPage.draft().moveOutlineItemToParent(childCardId, parentCardId)

        Fragment fragment = new Fragment("childFragment")

        when: "the outline item that is also a sub-item has a fragment added to it"
        workingPage.draft().addFragmentToOutlineItem(childCardId, fragment)

        then: "the outline item's and the corresponding sub-item on the parent outline item have a fragment added"
        DraftOutlineItem childOutlineItem = workingPage.draft().findOutlineItem(childCardId).get()
        DraftOutlineItem parentOutlineItem = workingPage.draft().findOutlineItem(parentCardId).get()
        childOutlineItem.hasFragment() && childOutlineItem.fragment() == fragment
        parentOutlineItem.subItems().get(0).hasFragment() && parentOutlineItem.subItems().get(0).fragment() == fragment
    }

    def "add a fragment to a root outline item"() {
        given: "a working page with a single root item without a fragment"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        WorkingCardId cardId = new WorkingCardId("1")
        String cardTitle = "Card"
        workingPage.addOutlineItemToRoot(cardId, cardTitle)

        Fragment fragment = new Fragment("rootFragment")

        when: "the outline item has a fragment added to it"
        workingPage.draft().addFragmentToOutlineItem(cardId, fragment)

        then: "the outline item's fragment and the corresponding sub-item on the root have the fragment added"
        DraftOutlineItem rootOutlineItem = workingPage.draft().findOutlineItem(cardId).get()
        rootOutlineItem.fragment() == fragment
        workingPage.draft().rootOutlineItem().subItems().get(0).fragment() == fragment
    }

    def "change an outline item's fragment"() {
        given: "a working page with a single root item with a fragment"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        WorkingCardId cardId = new WorkingCardId("1")
        String cardTitle = "Card"
        workingPage.addOutlineItemToRoot(cardId, cardTitle)

        Fragment fragment = new Fragment("rootFragment")
        workingPage.draft().addFragmentToOutlineItem(cardId, fragment)
        Fragment changedFragment = new Fragment("changedFragment")

        when: "the outline item has its fragment changed"
        workingPage.draft().changeOutlineItemFragment(cardId, changedFragment)

        then: "the outline item's fragment and the corresponding sub-item on the root have the fragment changed"
        DraftOutlineItem rootOutlineItem = workingPage.draft().findOutlineItem(cardId).get()
        rootOutlineItem.fragment() == changedFragment
        workingPage.draft().rootOutlineItem().subItems().get(0).fragment() == changedFragment
    }

    def "remove a fragment from a root outline item"() {
        given: "a working page with a single root item with a fragment"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        WorkingCardId cardId = new WorkingCardId("1")
        String cardTitle = "Card"
        workingPage.addOutlineItemToRoot(cardId, cardTitle)

        Fragment fragment = new Fragment("rootFragment")
        workingPage.draft().addFragmentToOutlineItem(cardId, fragment)

        when: "the outline item has its fragment removed"
        workingPage.draft().removeOutlineItemFragment(cardId)

        then: "the outline item's fragment and the corresponding sub-item on the root have the fragment removed"
        DraftOutlineItem rootOutlineItem = workingPage.draft().findOutlineItem(cardId).get()
        !rootOutlineItem.hasFragment()
        !workingPage.draft().rootOutlineItem().subItems().get(0).hasFragment()
    }

    def "remove an outline item that is at the root from a draft with multiple items at the root all of which have sub-items"() {
        given: "a working page with multiple items at the root; all with sub-items"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        List<WorkingCardId> parentCardIds = new ArrayList<>()
        Map<WorkingCardId, List<WorkingCardId>> parentIdToChildCardIds = new HashMap<>()
        WorkingCardId draftOutlineItemIdToRemove = new WorkingCardId("0")

        int currId = 0
        for (int x = 0; x < 4; x++) {
            WorkingCardId parentCardId = new WorkingCardId(Integer.toString(currId))
            parentCardIds.add(parentCardId)
            parentIdToChildCardIds.put(parentCardId, new ArrayList<WorkingCardId>())
            workingPage.addOutlineItemToRoot(parentCardId, "")
            currId++

            for (int y = 0; y < 4; y++) {
                WorkingCardId childCardId = new WorkingCardId(Integer.toString(currId))
                parentIdToChildCardIds.get(parentCardId).add(childCardId)
                workingPage.addOutlineItemToRoot(childCardId, "")
                workingPage.draft().moveOutlineItemToParent(childCardId, parentCardId)
                currId++
            }
        }

        when: "a parent item is removed"
        workingPage.removeOutlineItem(draftOutlineItemIdToRemove)

        then: "that parent item and all its sub-items were removed but all the others remain"
        parentCardIds.forEach(parentCardId -> {
            if (parentCardId == draftOutlineItemIdToRemove) {
                assert workingPage.draft().findOutlineItem(parentCardId).isEmpty()
                parentIdToChildCardIds.get(parentCardId).each(childCardId -> {
                    assert workingPage.draft().findOutlineItem(childCardId).isEmpty()
                })
            } else {
                assert workingPage.draft().findOutlineItem(parentCardId).isPresent()
                parentIdToChildCardIds.get(parentCardId).each(childCardId -> {
                    assert workingPage.draft().findOutlineItem(childCardId).isPresent()
                })
            }
        })
    }

    def "move a root outline item on a working page draft to a new (non-root) parent"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator,2, 2)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("3")).get()
        DraftOutlineItem cardToMoveTo = workingPage.draft().findOutlineItem(new WorkingCardId("0")).get()

        List<DraftOutlineSubItem> cardToMoveSubItems = cardToMove.subItems()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "a root outline item is moved to a non-root parent"
        workingPage.draft().moveOutlineItemToParent(cardToMove.id().workingCardId(), cardToMoveTo.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "the root no longer has the moved card as a sub-item" +
                "the outline item has the expected new parent;" +
                " the moved card still has all its sub-items"

        workingPage.draft().rootOutlineItem().subItems().stream().noneMatch(x -> x.workingCardId() == cardToMove.id().workingCardId())
        cardToMoveTo.subItems().stream().anyMatch(x -> x.workingCardId() == cardToMove.id().workingCardId())
        cardToMove.subItems().containsAll(cardToMoveSubItems)
    }

    def "move a non-root outline item on a working page draft to a new non-root parent"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 3, 2)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("1")).get()
        DraftOutlineItem cardToMoveParent = workingPage.draft().findOutlineItem(new WorkingCardId("0")).get()
        DraftOutlineItem cardToMoveTo = workingPage.draft().findOutlineItem(new WorkingCardId("7")).get()

        List<DraftOutlineSubItem> cardToMoveSubItems = cardToMove.subItems()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "an outline item is moved to a new non-root parent"
        workingPage.draft().moveOutlineItemToParent(cardToMove.id().workingCardId(), cardToMoveTo.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "the previous parent no longer has the moved card as a sub-item" +
                "the outline item has the expected new parent;" +
                " the moved card still has all its sub-items"

        cardToMoveParent.subItems().stream().noneMatch(x -> x.workingCardId() == cardToMove.id().workingCardId())
        cardToMoveTo.subItems().stream().anyMatch(x -> x.workingCardId() == cardToMove.id().workingCardId())
        cardToMove.subItems().containsAll(cardToMoveSubItems)
    }

    def "move a non-root outline item on a working page draft to the root of the draft"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 3, 2)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("1")).get()
        DraftOutlineItem cardToMoveParent = workingPage.draft().findOutlineItem(new WorkingCardId("0")).get()

        List<DraftOutlineSubItem> cardToMoveSubItems = cardToMove.subItems()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "an outline item is moved to the draft root"
        workingPage.draft().moveOutlineItemToRoot(cardToMove.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "the previous parent no longer has the moved card as a sub-item" +
                "the outline item has the root as its new parent;" +
                " the moved card still has all its sub-items"

        cardToMoveParent.subItems().stream().noneMatch(x -> x.workingCardId() == cardToMove.id().workingCardId())
        workingPage.draft().rootOutlineItem().subItems().stream().anyMatch(x -> x.workingCardId() == cardToMove.id().workingCardId())
        cardToMove.subItems().containsAll(cardToMoveSubItems)
    }

    def "try to move an outline item on a working page draft to a new parent it contains as a direct sub-item"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 2, 2)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("3")).get()
        DraftOutlineItem cardToMoveTo = workingPage.draft().findOutlineItem(new WorkingCardId("4")).get()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "trying to move an outline item to a parent it contains as a direct or inherited sub-item"
        workingPage.draft().moveOutlineItemToParent(cardToMove.id().workingCardId(), cardToMoveTo.id().workingCardId())

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error moving outline item to new parent; " +
                "the outline item contains the proposed new parent as a sub-item which introduces an infinite cycle"
    }

    def "try to move an outline item on a working page draft to a new parent it contains as an inherited sub-item"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator,3, 3)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("26")).get()
        DraftOutlineItem cardToMoveTo = workingPage.draft().findOutlineItem(new WorkingCardId("34")).get()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "trying to move an outline item to a parent it contains as a direct or inherited sub-item"
        workingPage.draft().moveOutlineItemToParent(cardToMove.id().workingCardId(), cardToMoveTo.id().workingCardId())

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error moving outline item to new parent; " +
                "the outline item contains the proposed new parent as a sub-item which introduces an infinite cycle"
    }

    def "try to move an outline item on a working page draft to its current parent"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator,2, 2)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("1")).get()
        DraftOutlineItem cardToMoveTo = workingPage.draft().findOutlineItem(new WorkingCardId("0")).get()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "trying to move an outline item to its current parent"
        workingPage.draft().moveOutlineItemToParent(cardToMove.id().workingCardId(), cardToMoveTo.id().workingCardId())

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error moving outline item to new parent; " +
                "the proposed new parent is already this outline items parent"
    }

    def "move a root outline item / card up"() {
        given: "a working page draft with multiple sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 1, 5)
        DraftOutlineItem cardToMoveUp = workingPage.draft().findOutlineItem(new WorkingCardId("3")).get()
        WorkingPageSpecHelper.printDraft(workingPage)

        when: "a card is moved up"
        workingPage.draft().moveOutlineItemUp(DraftOutlineItemId.createRootId(), cardToMoveUp.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it is moved up as expected"
        workingPage.draft().rootOutlineItem().subItems().get(2).workingCardId() == cardToMoveUp.id().workingCardId()
        workingPage.draft().outlineItemNumber(cardToMoveUp.id().workingCardId()).hierarchyNumber(".") == "3."
        workingPage.draft().outlineItemNumber(new WorkingCardId("2")).hierarchyNumber(".") == "4."
    }

    def "move a non-root outline item / card up"() {
        given: "a working page draft with multiple sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 3, 3)
        DraftOutlineItem cardToMoveUp = workingPage.draft().findOutlineItem(new WorkingCardId("22")).get()
        DraftOutlineItem cardToMoveUpParent = workingPage.draft().findOutlineItem(new WorkingCardId("13")).get()

        List<DraftOutlineSubItem> cardToMoveUpSubItems = cardToMoveUp.subItems()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "a non-root card is moved up"
        workingPage.draft().moveOutlineItemUp(cardToMoveUpParent.id(), cardToMoveUp.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it is moved up as expected; " +
                "it contains its original sub-items"
        cardToMoveUpParent.subItems().get(1).workingCardId() == cardToMoveUp.id().workingCardId()
        cardToMoveUp.subItems().containsAll(cardToMoveUpSubItems)
        workingPage.draft().outlineItemNumber(new WorkingCardId("22")).hierarchyNumber(".") == "2.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("23")).hierarchyNumber(".") == "2.2.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("24")).hierarchyNumber(".") == "2.2.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("25")).hierarchyNumber(".") == "2.2.3."
        workingPage.draft().outlineItemNumber(new WorkingCardId("18")).hierarchyNumber(".") == "2.3."
        workingPage.draft().outlineItemNumber(new WorkingCardId("19")).hierarchyNumber(".") == "2.3.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("20")).hierarchyNumber(".") == "2.3.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("21")).hierarchyNumber(".") == "2.3.3."
    }

    def "move a root outline item / card down"() {
        given: "a working page draft with multiple sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 1, 5)
        DraftOutlineItem cardToMoveDown = workingPage.draft().findOutlineItem(new WorkingCardId("3")).get()
        WorkingPageSpecHelper.printDraft(workingPage)

        when: "a root card is moved down"
        workingPage.draft().moveOutlineItemDown(DraftOutlineItemId.createRootId(), cardToMoveDown.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it is moved down as expected"
        workingPage.draft().rootOutlineItem().subItems().get(4).workingCardId() == cardToMoveDown.id().workingCardId()
        workingPage.draft().outlineItemNumber(cardToMoveDown.id().workingCardId()).hierarchyNumber(".") == "5."
        workingPage.draft().outlineItemNumber(new WorkingCardId("4")).hierarchyNumber(".") == "4."
    }

    def "try to make a non-root outline item on a working page draft its own parent"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 3, 2)

        DraftOutlineItem cardToMove = workingPage.draft().findOutlineItem(new WorkingCardId("1")).get()

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "trying to make an outline item its own parent"
        workingPage.draft().moveOutlineItemToParent(cardToMove.id().workingCardId(), cardToMove.id().workingCardId())
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Draft: cannot make an outline item its own parent"
    }

    def "try to add duplicate fragments to a working page draft under different outline items"() {
        given: "a working page draft with multiple layers and a fragment to add"

        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 2, 2)
        WorkingPageSpecHelper.printDraft(workingPage)

        Fragment fragment = new Fragment("fragment")

        when: "trying to add duplicate fragments across layers"
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("1"), fragment)
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("4"), fragment)
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Draft: adding this fragment results in duplicate fragments"
    }

    def "try to add duplicate fragments to a working page draft under a single outline item; to the parent and then to a sub-item"() {
        given: "a working page draft with multiple layers and a fragment to add"

        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 2, 2)
        WorkingPageSpecHelper.printDraft(workingPage)

        Fragment fragment = new Fragment("fragment")

        when: "trying to add duplicate fragments to a parent and one of its sub-items"
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("0"), fragment)
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("1"), fragment)
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Draft: adding this fragment results in duplicate fragments"
    }

    def "try to add duplicate fragments to a working page draft under a single outline item; to the sub-item and then to the parent"() {
        given: "a working page draft with multiple layers and a fragment to add"

        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 2, 2)
        WorkingPageSpecHelper.printDraft(workingPage)

        Fragment fragment = new Fragment("fragment")

        when: "trying to add duplicate fragments to a parent and one of its sub-items"
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("1"), fragment)
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("0"), fragment)
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Draft: adding this fragment results in duplicate fragments"
    }

    def "try to add duplicate fragments to a working page draft to outline items that aren't parent / child but one is under the other indirectly"() {
        given: "a working page draft with at least 3 layers and a fragment to add"

        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 3, 1)
        WorkingPageSpecHelper.printDraft(workingPage)

        Fragment fragment = new Fragment("fragment")

        when: "trying to add duplicate fragments to two outline items that aren't parent / child but one is under the other indirectly"
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("0"), fragment)
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("2"), fragment)
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Draft: adding this fragment results in duplicate fragments"
    }

    def "draft outline item numbers and levels"() {
        given: "a working page with several outline items and levels"

        WorkingPage workingPage = workingPageFactoryCollaborator.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))
        WorkingCardId id1 = new WorkingCardId("1")
        WorkingCardId id2 = new WorkingCardId("2")
        WorkingCardId id3 = new WorkingCardId("3")
        WorkingCardId id4 = new WorkingCardId("4")
        WorkingCardId id5 = new WorkingCardId("5")

        workingPage.addOutlineItemToRoot(id1, "Working Card")
        workingPage.addOutlineItemToRoot(id2, "Working Card")
        workingPage.addOutlineItemToRoot(id3, "Working Card")
        workingPage.addOutlineItemToRoot(id4, "Working Card")
        workingPage.addOutlineItemToRoot(id5, "Working Card")

        workingPage.draft().moveOutlineItemToParent(id5, id1)
        workingPage.draft().moveOutlineItemToParent(id4, id5)
        workingPage.draft().moveOutlineItemToParent(id3, id2)

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "querying for all the item numbers"

        ItemNumber itemNumber1 = workingPage.draft().outlineItemNumber(id1)
        ItemNumber itemNumber2 = workingPage.draft().outlineItemNumber(id2)
        ItemNumber itemNumber3 = workingPage.draft().outlineItemNumber(id3)
        ItemNumber itemNumber4 = workingPage.draft().outlineItemNumber(id4)
        ItemNumber itemNumber5 = workingPage.draft().outlineItemNumber(id5)

        then: "they are the expected values"
        itemNumber1.hierarchyNumber(".") == "1."
        itemNumber1.level() == 1
        itemNumber2.hierarchyNumber(".") == "2."
        itemNumber2.level() == 1
        itemNumber3.hierarchyNumber(".") == "2.1."
        itemNumber3.level() == 2
        itemNumber4.hierarchyNumber(".") == "1.1.1."
        itemNumber4.level() == 3
        itemNumber5.hierarchyNumber(".") == "1.1."
        itemNumber5.level() == 2
    }

    def "item numbers for moved items and impacted items are updated as expected"() {
        given: "a working page draft with layers"

        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 3, 2)
        WorkingPageSpecHelper.printDraft(workingPage)


        when: "moving an item with subitems to a different parent"
        workingPage.draft().moveOutlineItemToParent(new WorkingCardId("1"), new WorkingCardId("7"))
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "item numbers for the moved item its children and other impacted items are updated as expected"
        workingPage.draft().outlineItemNumber(new WorkingCardId("1")).hierarchyNumber(".") == "2.3."
        workingPage.draft().outlineItemNumber(new WorkingCardId("2")).hierarchyNumber(".") == "2.3.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("3")).hierarchyNumber(".") == "2.3.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("4")).hierarchyNumber(".") == "1.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("5")).hierarchyNumber(".") == "1.1.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("6")).hierarchyNumber(".") == "1.1.2."
    }

    def "item numbers for moved items and impacted items are updated as expected 2"() {
        given: "a working page draft with layers"

        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 3, 2)
        WorkingPageSpecHelper.printDraft(workingPage)


        when: "moving an item with subitems to a different parent"
        workingPage.draft().moveOutlineItemToParent(new WorkingCardId("1"), new WorkingCardId("7"))
        workingPage.draft().moveOutlineItemToParent(new WorkingCardId("7"), new WorkingCardId("0"))

        WorkingPageSpecHelper.printDraft(workingPage)

        then: "item numbers for the moved item its children and other impacted items are updated as expected"
        workingPage.draft().outlineItemNumber(new WorkingCardId("7")).hierarchyNumber(".") == "1.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("8")).hierarchyNumber(".") == "1.2.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("9")).hierarchyNumber(".") == "1.2.1.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("10")).hierarchyNumber(".") == "1.2.1.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("11")).hierarchyNumber(".") == "1.2.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("12")).hierarchyNumber(".") == "1.2.2.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("13")).hierarchyNumber(".") == "1.2.2.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("1")).hierarchyNumber(".") == "1.2.3."
        workingPage.draft().outlineItemNumber(new WorkingCardId("2")).hierarchyNumber(".") == "1.2.3.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("3")).hierarchyNumber(".") == "1.2.3.2."
    }

    def "item numbers are updated when moving a non-root outline item on a working page draft to the root of the draft"() {
        given: "a draft with multiple outline items with sub-items"
        WorkingPage workingPage = WorkingPageSpecHelper.workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 3, 2)

        WorkingPageSpecHelper.printDraft(workingPage)

        when: "an outline item is moved to the draft root"
        workingPage.draft().moveOutlineItemToRoot(new WorkingCardId("1"))
        WorkingPageSpecHelper.printDraft(workingPage)

        then: "the item numbers are updated as expected"
        workingPage.draft().outlineItemNumber(new WorkingCardId("1")).hierarchyNumber(".") == "3."
        workingPage.draft().outlineItemNumber(new WorkingCardId("2")).hierarchyNumber(".") == "3.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("3")).hierarchyNumber(".") == "3.2."
        workingPage.draft().outlineItemNumber(new WorkingCardId("4")).hierarchyNumber(".") == "1.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("5")).hierarchyNumber(".") == "1.1.1."
        workingPage.draft().outlineItemNumber(new WorkingCardId("6")).hierarchyNumber(".") == "1.1.2."
    }
}
