FROM ibm-semeru-runtimes:open-17-jre
COPY target/content-service-1.0.0-SNAPSHOT.jar content-service-1.0.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/content-service-1.0.0-SNAPSHOT.jar"]